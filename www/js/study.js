var selected_news;
var news;
var selected_tezaurus;
var user_country = 1;

function parseNews()
{
	$('.wrapper.news a:not(.template)').each(function(index, element) {
        $(element).remove();
    });
	
	if( localStorage.getItem("news") )
	{
		news = JSON.parse( localStorage.getItem("news") );
		
		$(news).each(function(index, element) {
			
			var currentTemplate = $('.wrapper.news .template').clone();
			
			$(currentTemplate).find('.preview').css('background-image', 'url(' + element.image + ')' );
			$(currentTemplate).find('.title').text( element.title );
			$(currentTemplate).find('.time').text( formatDate( element.created_at, 'date') );
			$(currentTemplate).attr('data-index', index);
			
			$(currentTemplate).on('click',function(){
				selected_news = index;
			});
			
			$(currentTemplate).removeClass('template');
			$(currentTemplate).appendTo('.wrapper.news');
		});
	}
}

function newsRequest()
{
	$.ajax({
		method: "GET",
		url: cordovaMode?"https://lk.els24.com/api/education":"echo_education.php",
		data: { 
			'token': localStorage.getItem('token')
		},
		timeout: 10000,
		beforeSend: function() {
			
		}
	})
	.done(function( msg ) {
		console.log( msg );
		if( msg.status == "success" )
		{
			localStorage.setItem("news", JSON.stringify(msg.data));
		}
	})
	.fail(function() {
	})
	.always(function() {
		parseNews();
	});
}

var nostraData;
var nostraReply = '';

function parseNostradamus(){
	if(localStorage.getItem('nostradamus')){
		var data = localStorage.getItem('nostradamus');

		nostraData = JSON.parse(data);
		console.log(nostraData);

		$('.nostra').html('');

		$(nostraData.categories).each(function(index, el) {
			$('.nostra').append('<div class="item"><img src="img/nostra_icons/'+el.id+'.svg"><div class="title" data-id="'+el.id+'" onclick="showNostraQuestions(this);">'+el.title+'</div><div class="questions"></div></div>');
		});
	}
}

function showNostraQuestions(obj){
	if ( $(obj).parent().find('.question').length > 0 ) {
		$(obj).parent().find('.questions').html('');
		$(obj).parent().find('.button, .empty').remove();
	}else{
		$(obj).parent().find('.questions').html('');

		$(nostraData.questions).each(function(index, el) {						
			if( parseInt( el.category_id ) == parseInt( $(obj).attr('data-id') ) && el.related == '0' ){				
				if (el.title!=null) {
					var html = '<div class="question ' + (el.multiselect === '1' ? 'multiselect' : '') + '" data-id="'+el.id+'"><div class="text">'+el.title+'</div><div class="answers">';
				}else{
					var html = '<div class="question ' + (el.multiselect === '1' ? 'multiselect' : '') + '" data-id="'+el.id+'"><div class="answers">';
				}

				$(nostraData.answers).each(function(index2, el2) {
					if ( parseInt(el2.question_id) == parseInt(el.id) ) {
						html += '<div class="option"><p onclick="selectNostraOption(this)" data-reply="'+el2.response+'" data-related-question-id="'+el2.related_question_id+'" data-id="'+el2.id+'">'+el2.title+'</p></div>';
					}
				});

				html += '</div></div>';

				$(obj).parent().find('.questions').append(html);
			}
		});

		$(obj).parent().append('<div class="button" onclick="getNostraAnswer(this);">Получить ответ</div><div onclick="emptyNostra(this);" class="empty">Очистить</div>');
	}	
}

function selectNostraOption(obj){
	if ($(obj).closest('.question').hasClass('multiselect')) {													
		$(obj).parent().find('.subquestion').remove();
		var needRenderSubquestion = !$(obj).parent().hasClass('selected');
		$(obj).parent().toggleClass('selected');
		if (!needRenderSubquestion) {						
			return;
		}				
	}	else {
		$(obj).parent().parent().find('.option').removeClass('selected');
		$(obj).parent().addClass('selected');
		$(obj).parent().parent().parent().find('.subquestion').remove();				
	}

	
	if ($(obj).attr('data-related-question-id') != '0') {
		$(nostraData.questions).each(function(index, el) {
			if ( parseInt(el.id) == parseInt($(obj).attr('data-related-question-id')) ) {
				
				var html ='<div class="subquestion">';

				if (el.title!=null) {
					html += '<div class="question ' + (el.multiselect === '1' ? 'multiselect' : '') + '" data-id="'+el.id+'"><div class="text">'+el.title+'</div><div class="answers">';
				}else{
					html += '<div class="question ' + (el.multiselect === '1' ? 'multiselect' : '') + '" data-id="'+el.id+'"><div class="answers">';
				}

				$(nostraData.answers).each(function(index2, el2) {
					if ( parseInt(el2.question_id) == parseInt($(obj).attr('data-related-question-id')) ) {
						html += '<div class="option"><p onclick="selectNostraOption(this)" data-reply="'+el2.response+'" data-related-question-id="'+el2.related_question_id+'" data-id="'+el2.id+'">'+el2.title+'</p></div>';
					}
				});

				html += '</div></div>';

				$(obj).parent().append(html);
			}
		});
	}
}

function getNostraAnswer(obj){
	nostraReply = '';

	$(obj).parent().find('.option.selected').each(function(index, el) {
		if( $(el).find('p').attr('data-reply').length>0 && $(el).find('p').attr('data-reply') != 'null' ){
			nostraReply += '<p>' + $(el).find('p').attr('data-reply') + '</p>';
		}
	});

	if(nostraReply.length==0){
		myApp.alert('Пожалуйста, ответьте на вопросы для получения ответа', 'Внимание', function () {});
	}else{
		$('.popup-nostradamus .caption').html('Ответ');
		$('.popup-nostradamus .answer').html(nostraReply);
		
		myApp.popup('.popup-nostradamus');

		$('.popup-nostradamus .holder').scrollTop(0);
	}
}

function emptyNostra(obj){
	$(obj).parent().find('.option').removeClass('selected');
	$(obj).parent().find('.subquestion').remove();
}

var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

myApp.onPageInit('study', function (page) {

	if( localStorage.getItem('country') && localStorage.getItem('country') == 'KZ' ){
		user_country = 3;
	}

	//nostradamus
	
	if (!localStorage.getItem('nostradamus')) {
		$.ajax({
			method: "POST",
			url: cordovaMode?"https://cp.els24.com/api/v1?token=Zt6HRAosYCh5au4JjQ8xJhkhJu0yF83yZ4FO6FK5&a=nsGetBase&country_id="+user_country+"&language_id=1":"echo_nostradamus.php",
			timeout: 300000,
			beforeSend: function() {
				//showLoading();
				//$('.nostra').html('')
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.status == "success" ){
				localStorage.setItem('nostradamus', JSON.stringify(msg) );
				parseNostradamus();
			}
			else{
			}
		})
		.fail(function() {
			myApp.alert('Для работы этого раздела требуется доступ к Интернет', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	}else{
		parseNostradamus();
	}

	//parseNews();
	
	//newsRequest();
	
	$('.navbar .exit').off('click');
	$('.navbar .exit').on('click',function(){
		myApp.confirm('Вы действительно хотите выйти?', 'Выход', 
			function () {
				localStorage.setItem('token',null);
				mainView.router.loadPage('login.html');
			},
			function () {
			}
		);
	});

	$('.popup-nostradamus .exit').on('click', function(event) {
		myApp.closeModal('.popup-nostradamus');
	});

	$('.popup-nostradamus .close').off('click');
	$('.popup-nostradamus .close').on('click', function(event) {
		
		var questions = [];

		$('.nostra .option.selected').each(function(index, el) {
			if($(el).find('p').attr('data-reply') != ''){
				questions.push( $(el).find('p').attr('data-id') );
			}
		});

		myApp.confirm('Отправить ответ на email?','Внимание',function(){
			$.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/nostradamus-send-answer":"echo_nostra_email.php",
				timeout: 10000,
				data: { 
					'token': localStorage.getItem('token'),
					//'id': JSON.stringify(questions)
					'id': questions
				},
				beforeSend: function() {
					showLoading();
					console.log(JSON.stringify(questions));
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" ){
					myApp.alert( msg.message, 'Спасибо', function () {
						myApp.closeModal('.popup-nostradamus');
					});
				}
				else{
					myApp.alert( msg.message, 'Ошибка сервера', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		})
	});

});