var session;
var callSound;

var beepTimer;
var audioSourceTimer;
var targetAudioSource = 'NORMAL';

if(myApp.device.android){
	if(premiumUser){
		var userAgent = new SIP.UA({
			uri: '1013@188.94.208.133',
			wsServers: 'wss://freeswitch.intellin.ru:7443',
			//authorizationUser: 'hot_els_',
			password: '1013',
			registrarServer: 'sip:188.94.208.133',
			autostart: false
		});
	}else{
		var userAgent = new SIP.UA({
			uri: '1013@188.94.208.133',
			wsServers: 'wss://freeswitch.intellin.ru:7443',
			//authorizationUser: 'hot_els_',
			password: '1013',
			registrarServer: 'sip:188.94.208.133',
			autostart: false
		});
	}
	

	var options = {
		media: {
			constraints: {
				audio: true,
				video: false
			},
			render: {
				remote: document.getElementById('remoteVideo'),
				local: document.getElementById('localVideo')
			}
		}
	};
}

function startCall() {
	$('#keyboard').css('opacity',0.5);
	$('#voicetarget').css('opacity',0.5);
	
	/*
	if(cordovaMode){
		permissions.hasPermission(permissions.VIBRATE, function( status ){
			if ( status.hasPermission ) {
				alert("Yes VIBRATE");
			}
			else {
				alert("No VIBRATE");
				permissions.requestPermission(permissions.VIBRATE);
			}
		});

		permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
			if ( status.hasPermission ) {
				alert("Yes WRITE_EXTERNAL_STORAGE");
			}
			else {
				alert("No WRITE_EXTERNAL_STORAGE");
				permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE);
			}
		});

	}
	*/
	
	var is_online = true;
					
	$.ajax({
		url: cordovaMode?"https://lk.urist.els24.com/api/education":"echo_education.php",
		timeout: 3000,
		statusCode: {
			0: function() {
				is_online = false;

				myApp.alert('Для совершения звонка требуется доступ к Интернет', 'Нет соединения с интернетом', function () {});
	
				call_has_started = false;

				$('#connectToSipButtonDiv div').removeClass('endCall');
				
				$('#keyboard').css('opacity',0);
				$('#speaker').css('opacity',0);
				
				callSound.pause();
				callSound.currentTime = 0;

				clearTimeout(beepTimer);
				clearInterval(audioSourceTimer);

				$('.popup-call .caller .callbutton.dtmf').removeClass('touchable');
			}
		}
	});
	
	if(!call_has_started && is_online){
		
		call_has_started = true;

		$('#connectToSipButtonDiv div').addClass('endCall');

		callSound = new Audio('sounds/zvuki_gudkov_telefona.wav');
		callSound.loop = true;
		callSound.play();

		beepTimer = setTimeout(function() {
			if(userAgent.isRegistered && call_has_started){
				
				callSound.pause();
				callSound.currentTime = 0;

				userAgent.start();
				
				var profile = JSON.parse( localStorage.getItem("profile") );
										
				if(premiumUser){
					session = userAgent.invite( 'hot_els_i_pr_' + profile.phone + '@188.94.208.133', options);
				}else{
					session = userAgent.invite( 'hot_els_i_' + profile.phone + '@188.94.208.133', options);
				}
				
				if(cordovaMode){
					AudioToggle.setAudioMode(AudioToggle.NORMAL);
				}
				
				$('.popup-call .caller .callbutton.dtmf').addClass('touchable');
			}
			else{
				callSound.pause();
				callSound.currentTime = 0;
			}
		}, 3000);

		audioSourceTimer = setInterval(function() {
			if( targetAudioSource == 'EARPIECE'){
				if(cordovaMode){
					AudioToggle.setAudioMode(AudioToggle.EARPIECE);
				}
			}else if( targetAudioSource == 'SPEAKER'){
				if(cordovaMode){
					AudioToggle.setAudioMode(AudioToggle.SPEAKER);
				}
			}else if( targetAudioSource == 'NORMAL'){
				if(cordovaMode){
					AudioToggle.setAudioMode(AudioToggle.NORMAL);
				}
			}
			else if( targetAudioSource == 'RINGTONE'){
				if(cordovaMode){
					AudioToggle.setAudioMode(AudioToggle.RINGTONE);
				}
			}
		}, 1000);

		userAgent.on('registered', function () {
			callSound.pause();
			callSound.currentTime = 0;
			if(cordovaMode){
				AudioToggle.setAudioMode(AudioToggle.NORMAL);
			}
		});
	}
}

function dtmf(number) {
	if(myApp.device.ios){
		window.session.dtmf(number);
	}else{
		session.dtmf(number);
	}
}

function endCall() {
	myApp.closeModal();
	
	$('#keyboard').css('opacity',0);
	$('#voicetarget').css('opacity',0);
	
	$('.precaller').fadeIn(300);
	
	call_has_started = false;
	
	$('#connectToSipButtonDiv div').removeClass('endCall');
	
	if(userAgent.isRegistered){
		session.bye();
	}
	
	callSound.pause();
	callSound.currentTime = 0;
	
	clearTimeout(beepTimer);
	clearInterval(audioSourceTimer);
	
	if(cordovaMode){
		if(myApp.device.android){
			AudioToggle.setAudioMode(AudioToggle.NORMAL);
		}else{
			AudioToggle.setAudioMode(AudioToggle.EARPIECE);
		}
	}
	
	targetAudioSource = 'NORMAL';
	
	$('.popup-call .caller .callbutton.dtmf').removeClass('touchable');
	//$('#earphone').css('opacity',1);
	//$('#speaker').css('opacity',0.5);
}

function earphone() {
	//$('#earphone').css('opacity',1);
	$('#voicetarget').css('opacity',0.5);
	
	if(cordovaMode){
		if(myApp.device.android){
			AudioToggle.setAudioMode(AudioToggle.NORMAL);
		}else{
			AudioToggle.setAudioMode(AudioToggle.EARPIECE);
		}
	}
	
	if(myApp.device.android && cordovaMode){
		targetAudioSource = 'NORMAL';
	}
}

function speaker() {
	//$('#earphone').css('opacity',0.5);
	$('#voicetarget').css('opacity',1);
	
	if(cordovaMode){
		AudioToggle.setAudioMode(AudioToggle.SPEAKER);
	}
	
	if(myApp.device.android && cordovaMode){
		targetAudioSource = 'SPEAKER';
	}
}

function switch_voice_target(){
	if(speaker_mode=='ear'){
		speaker();
		speaker_mode = 'speaker';
	}else{
		earphone();
		speaker_mode = 'ear';
	}
}

$('#popupCall').on('click', function () {
	if(localStorage.getItem('country') == 'RU'){
		var buttons = [
	        {
	            text: 'Выберите тип звонка',
	            label: true
	        },
	        {
	            text: 'Телефон',
				onClick: function(){
					if(localStorage.getItem('country') && localStorage.getItem('country') == 'KZ'){
						window.open("tel:" + (premiumUser?'8(800)0805090':'8(800)0805090') , "_system");
					}else{
						window.open("tel:" + (premiumUser?'8(800)7754401':'8(800)7757308') , "_system");
					}

					window.ga.trackEvent('Call', 'GSM');
				}
	        },
	        {
	            text: 'Интернет',
				onClick: function(){
					if(cordovaMode){
						window.ga.trackEvent('Call', 'SIP');
					}
					
					if(myApp.device.android){
						if(cordovaMode){
							var permissions = cordova.plugins.permissions;

							var list = [
								permissions.USE_SIP,
								permissions.MODIFY_AUDIO_SETTINGS,
								permissions.RECORD_AUDIO
							];

							permissions.requestPermissions(list, permissionsOk, permissionsError);

							function permissionsOk(){
								$('.popup-call .caller .callbutton.dtmf').removeClass('touchable');
								myApp.popup('.popup-call');
							}

							function permissionsError(){

							}
						}else{
							$('.popup-call .caller .callbutton.dtmf').removeClass('touchable');
							myApp.popup('.popup-call');
						}
					}else{
						$('.popup-call .caller .callbutton.dtmf').removeClass('touchable');
						myApp.popup('.popup-call');
					}
				}
	        },
			{
	            text: 'Отмена',
	            color: 'red'
	        }
	    ];

	    myApp.actions(buttons);
	}else{
		//if(localStorage.getItem('country') && localStorage.getItem('country') == 'KZ'){
		if(true){
			window.open("tel:" + (premiumUser?'8(800)0805090':'8(800)0805090') , "_system");
		}else{
			window.open("tel:" + (premiumUser?'8(800)7754401':'8(800)7757308') , "_system");
		}

		window.ga.trackEvent('Call', 'GSM');
	}
});

document.addEventListener("pause", onPause, false);

function onPause() {
	$('#closeCallerPopup').click();
	
	if(myApp.device.ios){
		window.userAgent.unregister();
	}else{
		userAgent.unregister();
		AudioToggle.setAudioMode(AudioToggle.NORMAL);
	}
	
	var now = new Date();
	now = now.toString();
	localStorage.setItem("logoutdate", now);
}

$('#keyboard').on('click',function(){
	if( $(this).css('opacity')==0.5 ){
		$(this).css('opacity',1);
		$('.precaller').fadeOut(300);
	}else{
		$(this).css('opacity',0.5);
		$('.precaller').fadeIn(300);
	}
});