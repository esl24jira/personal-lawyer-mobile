function parseConsultations() {
	if (localStorage.getItem("consultations")) {
		consultations = JSON.parse(localStorage.getItem("consultations"));

		if (consultations.total_comments_count > 0) {
			$('.consultations-count').text(consultations.total_comments_count);
			$('.consultations-count').css('display', 'block');
		} else {
			$('.consultations-count').css('display', 'none');
		}

		consultations = consultations.data;

		$('.consultations .consultation').remove();
		$('.showPreloader').remove();

		if (consultations.length == 0) {
			//$('.consultations').html('<div style="text-align:center; font-size: 14px; font-weight: bold;">Нет текущих консультаций</div>');
			$('.noConsultations').css('display', 'block');
		} else {
			$('.noConsultations').css('display', 'none');
		}

		$(consultations).each(function (index, element) {
			var currentTemplate = $('.consultations .template').clone();

			$(currentTemplate).find('.status').text(element.status_title);
			if (element.status_id == 6) {
				$(currentTemplate).find('.status').addClass('cancelled');
			} else if (element.status_id == 5) {
				$(currentTemplate).find('.status').addClass('finished');
			}

			$(currentTemplate).find('.type').text(element.services_title);

			if (element.description != null) {
				$(currentTemplate).find('.description').text(element.description);
			} else {
				$(currentTemplate).find('.description').remove();
			}

			$(currentTemplate).find('.num').text("№ " + element.id);
			$(currentTemplate).find('.date').text(convert_date_for_consultation(element.created_at_ts));

			var mess_count = '<span>' + (parseInt(element.comments_count)) + " сообщений";
			if (parseInt(element.comments_unread_count) > 0) {
				mess_count += ' <a class="newmessagescount">' + element.comments_unread_count + '</a>';
			}
			mess_count += '</span>';

			$(currentTemplate).find('.messages').html(mess_count);
			$(currentTemplate).attr('data-id', element.id);

			$(currentTemplate).on('click', function () {
				//if( element.rating )
				if (false) {
					selected_consultation = element.id;
					// mainView.router.loadPage('consultation.html');
				} else {
					if (localStorage.getItem("mb") == 0) {
						var targetUrl = 'https://lk.urist.els24.com/api/single-consultation';
					} else {
						var targetUrl = 'https://lk.urist.els24.com/api/mb-single-consultation';
					}

					$.ajax({
							method: "GET",
							url: cordovaMode ? targetUrl : "consul_one_echo.php",
							data: {
								'token': localStorage.getItem('token'),
								'task_id': element.id
							},
							timeout: 10000,
							beforeSend: function () {
								showLoading();
							}
						})
						.done(function (msg) {
							console.log(msg);
							if (msg.status == "success") {
								$(consultations).each(function (index2, element2) {
									if (element2.id == msg.data.task.id) {
										element2.comments = msg.data.comments;
										element2.description = msg.data.task.description;
										element2.rating = msg.data.task.rating;
										element2.low_country_title = msg.data.task.low_country_title;
										element2.rating = msg.data.task.rating;
										element.card_code = msg.data.task.card_code;
										element2.access_setting = msg.data.access_setting;

										if (msg.data.relationships) {
											$(msg.data.relationships).each(function (index3, element3) {
												if (element3.services_id == 68) {
													element2.blagodarnost = element3;
												}

												if (element3.services_id == 67) {
													element2.pretenziya = element3;
												}
											});
										}

										consultations[index2] = element2;

										current_consultation = element2;
										selected_consultation = element2.id;
									}
								});

								localStorage.setItem("consultations", JSON.stringify(consultations));
								console.log(current_consultation);
								// mainView.router.loadPage('consultation.html');
								mainView.router.loadPage('chat.html');
							} else if (msg.status == "failure" && msg.message == 'user not found') {
								localStorage.setItem("token", null);

								mainView.router.loadPage({
									url: 'login.html',
									animatePages: true
								});
							}
						})
						.fail(function () {
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						})
						.always(function () {
							hideLoading();
						});
				}


			});

			$(currentTemplate).removeClass('template');
			$(currentTemplate).addClass('consultation');
			$(currentTemplate).appendTo('.consultations');
		});
	} else {
		$('.consultations').html('<div style="text-align:center; font-size: 14px; font-weight: bold;">Нет текущих консультаций</div>');
	}
}

function getConsultations() {
	if (localStorage.getItem("mb") == 0) {
		var targetUrl = 'https://lk.urist.els24.com/api/consultation';
	} else {
		var targetUrl = 'https://lk.urist.els24.com/api/mb-consultation';
	}

	$.ajax({
			method: "GET",
			url: cordovaMode ? targetUrl : "consul_echo.php",
			data: {
				'token': localStorage.getItem('token')
			},
			timeout: 10000,
			beforeSend: function () {
				//parseConsultations();
			}
		})
		.done(function (msg) {
			console.log(msg);
			if (msg.status == "success") {
				localStorage.setItem("consultations", JSON.stringify(msg));
				//parseConsultations();
			} else if (msg.status == "failure" && msg.message == "user not found") {
				localStorage.setItem("token", null);
				mainView.router.loadPage('login.html');
			}
		})
		.fail(function () {})
		.always(function () {
			parseConsultations();
			myApp.pullToRefreshDone();
		});
}

myApp.onPageInit('consultations', function (page) {

	userLoggedIn = true;

	setTimeout(function () {

		$.ajax({
				method: "GET",
				url: cordovaMode ? "https://lk.urist.els24.com/api/promo-action" : "echo_promo.php",
				data: {
					token: localStorage.getItem('token')
				},
				timeout: 30000,
				beforeSend: function () {}
			})
			.done(function (msg) {
				console.log(msg);
				if (msg.status == "success" && msg.data.id) {
					var actionStart = Date.parse(msg.data.begin_at);
					var actionEnd = Date.parse(msg.data.end_at);
					var now = new Date();
					var promo_id = msg.data.id;
					var promo_card_id = msg.data.card_id;

					if ((!localStorage.getItem('promo_id_shown') || localStorage.getItem('promo_id_shown') != promo_id)) {
						//обновление карт
						$.ajax({
								method: "GET",
								url: cordovaMode ? "https://lk.urist.els24.com/api/cards" : "echo_cards.php",
								data: {
									'token': localStorage.getItem('token')
								},
								timeout: 10000,
								beforeSend: function () {
									//showLoading();
								}
							})
							.done(function (msg2) {
								console.log(msg2);
								if (msg2.status == "success") {
									localStorage.setItem("cards", JSON.stringify(msg2.data));

									$('.popup.promo span').text(localStorage.getItem('name'));

									$('.popup.promo .button').off('click');
									$('.popup.promo .button').on('click', function (event) {

										$(msg2.data).each(function (index, el) {
											if (el.id == promo_card_id) {
												selected_card_to_get_consultation = index;
											}
										});

										$('.page[data-page=consultations] .button.big').click();
										myApp.closeModal();

										localStorage.setItem('promo_id_shown', promo_id);
									});

									myApp.popup('.popup.promo');
								}
							})
							.fail(function () {})
							.always(function () {});
					}
				}
			})
			.fail(function () {})
			.always(function () {});
	}, 6000);

	selected_card_to_get_consultation = null;

	$.ajax({
			method: "GET",
			url: cordovaMode ? "https://urist.els24.com/geo/index.php" : "echo_geoip.php",
			timeout: 30000,
			beforeSend: function () {}
		})
		.done(function (msg) {
			console.log(msg);
			if (msg.result == "ok") {
				localStorage.setItem("country", msg.data);
			}
		})
		.fail(function () {})
		.always(function () {});

	//parseConsultations();
	$('.toolbar a.menu').each(function (index2, element2) {
		$(element2).find('img').attr('src', $(element2).attr('data-bg'));
	});

	$('.toolbar a.menu:first-of-type').find('img').attr('src', $('.toolbar a.menu:first-of-type').attr('data-active-bg'));

	$('.toolbar a.menu').removeClass('active');
	$('.toolbar a.menu:first-of-type').addClass('active');

	// show_zastavka();

	getConsultations();

	$.ajax({
			method: "GET",
			url: cordovaMode ? "https://lk.urist.els24.com/api/cards" : "echo_cards.php",
			data: {
				'token': localStorage.getItem('token')
			},
			timeout: 5000,
			beforeSend: function () {
				showLoading();
			}
		})
		.done(function (msg) {
			console.log(msg);
			if (msg.status == "success") {
				localStorage.setItem("cards", JSON.stringify(msg.data));
			}
		})
		.fail(function () {})
		.always(function () {
			if (localStorage.getItem('cards')) {
				var cards = JSON.parse(localStorage.getItem("cards"));

				premiumUser = false;
				$(cards).each(function (index, element) {
					if (element.package_id != 678) {
						premiumUser = true;
					}

					if (element.status_id == 2) {
						hasActivatedCard = true;
					}
				});
			}
			hideLoading();
		});

	$.ajax({
			method: "GET",
			url: cordovaMode ? "https://lk.urist.els24.com/api/profile" : "echo_profile.php",
			data: {
				'token': localStorage.getItem('token')
			},
			timeout: 10000,
			beforeSend: function () {

			}
		})
		.done(function (msg) {
			console.log(msg);
			if (msg.status == "success") {
				localStorage.setItem("profile", JSON.stringify(msg.data));
			}
		})
		.fail(function () {})
		.always(function () {});

	$('.navbar .exit').off('click');
	$('.navbar .exit').on('click', function () {
		myApp.confirm('Вы действительно хотите выйти?', 'Выход',
			function () {
				localStorage.setItem('token', null);
				mainView.router.loadPage('login.html');
			},
			function () {}
		);
	});

	$('.page[data-page=consultations] .button.big').on('click', function () {

		$('#newyear').fadeOut('1', function () {});

		if (!localStorage.getItem('getConsultationClicked')) {
			localStorage.setItem('getConsultationClicked', true);
			$('.page[data-page=consultations] .button.big').removeClass('animated');
		}

		if (localStorage.getItem("cards")) {
			cards = JSON.parse(localStorage.getItem("cards"));
			if (cards.length > 0) {
				if (!hasActivatedCard) {
					myApp.alert('Необходимо активировать хотя бы одну карту', 'Ошибка', function () {});
				} else {
					mainView.router.loadPage('get_consultation.html');
				}
			} else {
				myApp.modal({
					title: 'Купить карту',
					text: 'Для получения консультации вам необходимо приобрести карту',
					buttons: [{
						text: 'Купить карту',
						onClick: function () {
							if (localStorage.getItem("mb") == 0) {
								var targetUrl = 'https://lk.urist.els24.com/api/product';
							} else {
								var targetUrl = 'https://lk.urist.els24.com/api/mb-product';
							}
							$.ajax({
									method: "GET",
									url: cordovaMode ? "https://urist.els24.com/geo/index.php" : "echo_geoip.php",
									timeout: 30000,
									beforeSend: function () {
										showLoading();
									}
								})
								.done(function (msg) {
									console.log(msg);
									if (msg.result == "ok") {
										localStorage.setItem("country", msg.data);

										$.ajax({
												method: "GET",
												url: cordovaMode ? targetUrl : "echo_products.php",
												data: {
													country: msg.data
												},
												timeout: 10000,
												beforeSend: function () {
													showLoading();
												}
											})
											.done(function (msg) {
												console.log(msg);
												if (msg.status == "success") {
													localStorage.setItem("products", JSON.stringify(msg.data));
													buy_mode = 2;
													mainView.router.loadPage({
														url: 'buy.html',
														animatePages: true
													});
												} else {
													myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
												}
											})
											.fail(function () {
												myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
											})
											.always(function () {
												hideLoading();
											});
									} else {
										myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
									}
								})
								.fail(function () {
									myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
								})
								.always(function () {
									hideLoading();
								});
						}
					}]
				})
			}
		} else {
			$.ajax({
					method: "GET",
					url: cordovaMode ? "https://lk.urist.els24.com/api/cards" : "echo_cards.php",
					data: {
						'token': localStorage.getItem('token')
					},
					timeout: 10000,
					beforeSend: function () {
						showLoading();
					}
				})
				.done(function (msg) {
					console.log(msg);
					if (msg.status == "success") {
						//cards = msg.data;
						localStorage.setItem("cards", JSON.stringify(msg.data));

						if (msg.data.length > 0) {
							if (!hasActivatedCard) {
								myApp.alert('Необходимо активировать хотя бы одну карту', 'Ошибка', function () {});
							} else {
								mainView.router.loadPage('get_consultation.html');
							}
						} else {
							myApp.alert('Карт не найдено', 'Ошибка', function () {});
						}
					}
				})
				.fail(function () {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				})
				.always(function () {
					hideLoading();
				});
		}
	});

	if (localStorage.getItem('getConsultationClicked')) {
		$('.page[data-page=consultations] .button.big').removeClass('animated');
	}


	var ptrContent = $$('.pull-to-refresh-content');

	ptrContent.on('ptr:refresh', function (e) {
		setTimeout(function () {
			getConsultations();
		}, 500);
	});
});

myApp.onPageAfterAnimation('consultations', function (page) {
	hideBg();
	$('.window.pretenziya').fadeOut();
	$('.window.blagodarnost').fadeOut();
	$('.listview.services').fadeOut();

	var winterStart = Date.parse('Dec 25, 2017');
	var winterEnd = Date.parse('Jan 15, 2018');
	var now = new Date()

	if (now >= winterStart && now <= winterEnd) {
		$('#newyear').fadeIn(300);
	}
});

function convert_date_for_consultation(date) {
	date = date.split(' ');
	date = date[0].split('-');
	var output = date[2] + '.' + date[1] + '.' + date[0];
	return output;
}