var cardsSwiper;

function buyAddService(id, price){
	myApp.confirm('Купить услугу за ' + price + 'руб.?', 'Покупка услуги', function () {
		if( localStorage.getItem("mb") == 1){
			
			var buttons = [
			{
				text: 'Выберите тип оплаты',
				label: true
			},
			{
				text: 'Корпоративной картой',
				onClick: function(){
					$.ajax({
						method: "POST",
						url: cordovaMode?"https://lk.urist.els24.com/api/mb-order":"echo_order.php",
						timeout: 10000,
						data: { 
							'token': localStorage.getItem('token'),
							'id': id
						},
						beforeSend: function() {
							showLoading();
						}
					})
					.done(function( msg ) {
						console.log( msg );
						if( msg.status == "success" && msg.paymentUrl != null )
						{
							myApp.alert('Сейчас Вы будете направлены на страницу оплаты', 'Оплата покупки', function () {
								var ref = cordova.InAppBrowser.open( msg.paymentUrl, '_blank', 'closebuttoncaption=Готово');

								 ref.addEventListener('exit', function(){
									 mainView.router.loadPage({
										url: 'cards.html',
										animatePages: true
									});
								 });
							});
						}
						else{
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						}
					})
					.fail(function() {
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					})
					.always(function() {
						hideLoading();
					});
				}
			},
			{
				text: 'Выставить счет',
				onClick: function(){
					$.ajax({
						method: "POST",
						url: cordovaMode?"https://lk.urist.els24.com/api/mb-order-invoice":"echo_order.php",
						timeout: 10000,
						data: { 
							'token': localStorage.getItem('token'),
							'id': id
						},
						beforeSend: function() {
							showLoading();
						}
					})
					.done(function( msg ) {
						console.log( msg );
						if( msg.status == "success" )
						{
							myApp.alert('Счет на оплату сформирован и будет отправлен Вам на email', 'Спасибо', function () {
								mainView.router.loadPage({
									url: 'cards.html',
									animatePages: true
								});
							});
						}
						else{
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						}
					})
					.fail(function() {
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					})
					.always(function() {
						hideLoading();
					});
				}
			},
			{
				text: 'Отмена',
				color: 'red'
			}
		];

		myApp.actions(buttons);
			
		}else{
			$.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/order":"echo_order.php",
				timeout: 10000,
				data: { 
					'token': localStorage.getItem('token'),
					'id': id
				},
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" && msg.paymentUrl != null )
				{
					myApp.alert('Сейчас Вы будете направлены на страницу оплаты', 'Оплата покупки', function () {
						var ref = cordova.InAppBrowser.open( msg.paymentUrl, '_blank', 'closebuttoncaption=Готово');

						 ref.addEventListener('exit', function(){
							 mainView.router.loadPage({
								url: 'cards.html',
								animatePages: true
							});
						 });
					});
				}
				else{
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		}
	});
}

function parseDate(inputDate)
{
	if(inputDate)
	{
		var data = inputDate.split(" ");
		var data = data[0].split("-");
		var output = data[2] + "." + data[1] + "." + data[0];
	}
	else
	{
		var output = "-";
	}
	
	return output;
}

function setCardDescription(num)
{
	if( cards[num] ){
		$('.buyNewCardMessage').fadeOut(300, function() {});

		selected_card_to_get_consultation = num;

		$('div.cardnum').text( cards[num].code );
		$('div.cardnum').attr('data-card-id', cards[num].id );
		$('div.tarif').text( cards[num].package_title );
		$('div.code').text( cards[num].code );
		if( cards[num].status_id == 1 ){
			$('div.expire').text( "Карта не активирована " );
		}else{
			$('div.expire').html( 'Действует до <div class="date">'+parseDate( cards[num].expired_time )+'</div>' );
		}
		$('span.servicesCount').text( cards[num].services.length );


		$('.cardservices').html("");
		$(cards[num].services).each(function(index, element) {
			var service = '<div class="item">';

			service += '<div class="caption"><a href="#" data-description="' + element.description + '" data-service-id="' + element.id + '" class="serviceHintIcon"><img src="img/info-button (1).svg"></a>' + element.title + '</div>';
			
			service += '<div class="quantity">' + element.count + '</div>';

			if(element.available == 0 && element.purchase_id != null && element.purchase_price != 0){
				service += '<div class="button buyService" onclick="buyAddService(' + element.purchase_id + ',' + element.purchase_price + ');">Купить</div>';
			}else{
				service += '<div class="button getService" onclick="getService(\'' + element.title + '\',' + element.id + ');">Получить</div>';
			}

			service += '</div>';

			$('.cardservices').append(service);
		});

		$('#allUsers').html('');

		if( localStorage.getItem("users") )
		{
			var users = JSON.parse( localStorage.getItem("users") );
			
			if( users.users.length > 0 ){
				//$('.userlist').removeClass('hideit');
				var usersLinkedToCard = 0;
				$(users.users).each(function(index,element){
					if(element.card_id == cards[num].id){
						$('#allUsers').append('<div class="carduser"><img src="img/settings-user-remove.svg" class="remove" onclick="remove_cardholder(' + element.id + ');"><div class="name">' + element.first_name + '</div><div class="phone">' + element.phone + '</div><div class="email">' + element.email + '</div></div>');
						usersLinkedToCard++;
					}
				});
			}
		
			if(usersLinkedToCard<3){
				$('.addUser').removeClass('hideit');
			}else{
				$('.addUser').addClass('hideit');
			}
		}
		
		$('.page[data-page=cards] .wrapper .button.orange').css('display', ( (cards[num].status_id==2)?'none':'table') );
	
		$('.page[data-page=cards] .wrapper').scrollTop(0);
	}else{
		if(cards.length>0){
			$('.buyNewCardMessage').fadeIn(300, function() {});
		}
	}
	
	$('.serviceHintIcon').on('click',function(){
		$('.popover.popover-card-services-description-with-get .content-block').text( $(this).attr('data-description') );
		//$('.popover.popover-card-services-description-with-get').attr('data-consultation-type', $(this).parent().text() );
		//$('.popover.popover-card-services-description-with-get').attr('data-consultation-id', $(this).attr('data-service-id') );

		var clickedLink = this;
		myApp.popover('.popover-card-services-description-with-get', clickedLink );
	});
}

function getService(name,id){
	selected_consultation_to_get_consultation = { 
		'name' : name,
		'id' : id
	};

	mainView.router.loadPage({
		url: 'get_consultation.html',
		animatePages: true
	});

	myApp.closeModal();
}

function setCards()
{
	if( localStorage.getItem("cards") )
	{
		cardsSwiper.removeAllSlides();
		
		cards = JSON.parse( localStorage.getItem("cards") );
	
		$(cards).each(function(index, element) {
			cardsSwiper.appendSlide( '<div class="swiper-slide"><img src="' + element.image + '"></div>' );
		});

		cardsSwiper.appendSlide( '<div class="swiper-slide"><div class="buyNewCard"><div class="cell"><div onClick="openBuyCards();" class="button buyCards">Купить карту</div></div></div></div>' );
		
		setCardDescription(0);
		
		if( cards.length == 0 ){
			$('.page[data-page=cards] .wrapper').html('');
			$('.page[data-page=cards] .preloader').remove();
			//$('.cardslist').html('<table style="height:100%; width:100%; color:white; font-size:16px;"><tr><td align="center" valign="middle">Карт не найдено</table>');
			$('.noCards').css('display','block');
		}else{
			$('.noCards').css('display','none');
		}
	}
}

function openBuyCards(){
	$.ajax({
		method: "GET",
		url: cordovaMode?"https://urist.els24.com/geo/index.php":"echo_geoip.php",
		timeout: 10000,
		beforeSend: function() {
			showLoading();
		}
	})
	.done(function( msg ) {
		console.log( msg );
		if( msg.result == "ok" )
		{
			localStorage.setItem("country", msg.data);

			if( localStorage.getItem("mb") == 0){
				var targetUrl = 'https://lk.urist.els24.com/api/product?country=' + msg.data;
			}else{
				var targetUrl = 'https://lk.urist.els24.com/api/mb-product?country=' + msg.data;
			}

			$.ajax({
				method: "GET",
				url: cordovaMode?targetUrl:"echo_products.php?country=" + msg.data,
				timeout: 10000,
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" )
				{
					localStorage.setItem("products", JSON.stringify(msg.data));
					buy_mode = 2;
					mainView.router.loadPage({
						url: 'buy.html',
						animatePages: true
					});
				}
				else{
					if( localStorage.getItem("products") ){
						mainView.router.loadPage({
							url: 'buy.html',
							animatePages: true
						});
					}else{
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					}
					
				}
			})
			.fail(function() {
				if( localStorage.getItem("products") ){
					buy_mode = 2;
					mainView.router.loadPage({
						url: 'buy.html',
						animatePages: true
					});
				}else{
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				}
			})
			.always(function() {
				hideLoading();
			});
		}
		else{
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		}
	})
	.fail(function() {
		myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
	})
	.always(function() {
		hideLoading();
	});	
}

function cardRequest()
{
	$.ajax({
		method: "GET",
		url: cordovaMode?"https://lk.urist.els24.com/api/cards":"echo_cards.php",
		data: { 
			'token': localStorage.getItem('token')
		},
		timeout: 10000,
		beforeSend: function() {
			showLoading();
		}
	})
	.done(function( msg ) {
		console.log( msg );
		if( msg.status == "success" )
		{
			localStorage.setItem("cards", JSON.stringify(msg.data));

			$.ajax({
				method: "GET",
				url: cordovaMode?"https://lk.urist.els24.com/api/family-users":"echo_get_family_users.php",
				data: { 
					'token': localStorage.getItem('token')
				},
				timeout: 10000,
				beforeSend: function() {
					
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" )
				{
					localStorage.setItem("users", JSON.stringify(msg));
				}
			})
			.fail(function() {
			})
			.always(function() {
				setCards();
				hideLoading();
			});
		}
	})
	.fail(function() {
		hideLoading();
	})
	.always(function() {
		//setCards();
	});

	
}

myApp.onPageInit('cards', function (page) {
	cardsSwiper = myApp.swiper('.swiper-container3', {
		pagination:'.swiper-pagination3',
		spaceBetween: -( ($(document).width() - 223) / 1.25 ),
		onSlideChangeEnd: function(swiper){
			setCardDescription(swiper.activeIndex);
		}
	});

	selected_card_to_get_consultation = null;

	$('.addUser').on('click', function(event) {
		myApp.alert('Ваши вопросы не будут доступны другим пользователям по вашей карте, а их вопросы не будут доступны вам.', 'Внимание', function () {
			selected_card_for_user_to_add = cards[cardsSwiper.activeIndex];

			mainView.router.loadPage({
				url: 'add_cardholder.html',
				animatePages: true
			});
		});
	});
		
	//var ptrContent = $$('.page[data-page=cards] .pull-to-refresh-content');
/*
	ptrContent.on('ptr:refresh', function (e) {
		setTimeout(function () {
			cardRequest();
			myApp.pullToRefreshDone();
		}, 500);
	});
*/
	$('.navbar .exit').on('click',function(){
		localStorage.setItem('token',null);
		mainView.router.loadPage('login.html');
	});
	
	setCards();
	
	cardRequest();
	
	$('.buyCards').on('click',function(){
		openBuyCards();
	});
	
	$('.page[data-page=cards] .wrapper .button.orange').on('click',function(){
		
		var cardNumber = $(this).parent().find('.cardnum').attr('data-card-id');
		var buttonObj = $(this);
		
		
		myApp.confirm('Активировать карту?', 'Активация карты', 
			function () {
				$.ajax({
					method: "GET",
					url: cordovaMode?"https://lk.urist.els24.com/api/activate-card":"echo_activate.php",
					data: { 
						'token': localStorage.getItem('token'),
						'card_id': cardNumber
					},
					timeout: 10000,
					beforeSend: function() {
						showLoading();
					}
				})
				.done(function( msg ) {
					console.log( msg );
					if( msg.status == "success" )
					{
						$(buttonObj).css('display','none');

						cardRequest();
						//$('.page[data-page=cards] .wrapper .button.orange').css('display','none');
						//$('.page[data-page=cards] .wrapper .date').text('Карта активирована');
						
						hasActivatedCard = true;

						myApp.alert('Карта активирована', 'Спасибо', function () {});
					}
					else{
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					}
				})
				.fail(function() {
				})
				.always(function() {
					hideLoading();
				});
			},
			function () {
			}
		);
	});

	$('.servicess').on('click', function(event) {
		myApp.popup('.popup-services');
	});

	$('.popup-services .close').on('click', function(event) {
		myApp.closeModal();
	});
});

function remove_cardholder(user_id){
	myApp.confirm('Удалить пользователя?', 'Подтвердите удаление',
		function () {
			$.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/family-users-delete":"echo_family_users_delete.php",
				data: { 
					'token': localStorage.getItem('token'),
					'id': user_id
				},
				timeout: 10000,
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log(msg);
				if( msg.status == 'success' )
				{
					myApp.alert('Пользователь удален', 'Спасибо', function () {});
					cardRequest();
				}
				else if( msg.status == 'failure' ){
					myApp.alert(msg.message, 'Ошибка', function () {});
				}
				else
				{
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		},
		function () {
		}
	);
}