myApp.onPageInit('enter', function (page) {
	$('#buyCertificate').on('click',function(){
		$.ajax({
			method: "GET",
			url: cordovaMode?"https://urist.els24.com/geo/index.php":"echo_geoip.php",
			timeout: 30000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.result == "ok" )
			{
				localStorage.setItem("country", msg.data);

				$.ajax({
					method: "GET",
					url: cordovaMode?("https://lk.urist.els24.com/api/product?country=" + msg.data):"echo_products.php?country=KZ",
					data: {
						country: msg.data
					},
					timeout: 10000,
					beforeSend: function() {
						showLoading();
					}
				})
				.done(function( msg ) {
					console.log( msg );
					if( msg.status == "success" )
					{
						localStorage.setItem("products", JSON.stringify(msg.data));
						mainView.router.loadPage({
							url: 'buy.html',
							animatePages: true
						});
					}
					else{
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					}
				})
				.fail(function() {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				})
				.always(function() {
					hideLoading();
				});
			}
			else{
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	});

	$('.page[data-page=enter] .button.reg').on('click',function(){
		
		register_without_card = true;

		$.ajax({
			method: "GET",
			url: cordovaMode?"https://urist.els24.com/geo/index.php":"echo_geoip.php",
			timeout: 30000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.result == "ok" )
			{
				localStorage.setItem("country", msg.data);

				mainView.router.loadPage({
					url: 'anketa.html',
					animatePages: true
				});
			}
			else{
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	});
});