function show_zastavka(){
	console.log(zastavka_shown);
	if(!zastavka_shown){
		if( userFirstName ){
			$('.zastavka .name p').text( userFirstName );
		}else{
			$('.zastavka .name').each(function(index,element){
				$(element).text( $(element).text().replace(',','') );
			})
		}

		var d = new Date();
		var h = d.getHours();

		$('.zastavka').css('opacity',1);

		if(h>=12 && h<18){
		
			$('.zastavka.morning, .zastavka.day').css('display','block');
			$('.zastavka.morning .moon').css('display','none');
			
			$('.zastavka.morning').css('z-index','99990');
			$('.zastavka.morning .name').remove();
			
			setTimeout(function(){
				$('.zastavka.morning').css('opacity',0);
			},500);

			setTimeout(function(){
				$('.sun_offset').css('transform','rotate3d(0,0,1,0deg)');
			},1000);

			setTimeout(function(){
				$('.zastavka').fadeOut(300,'linear',function() {
					setTimeout(function(){
						$('.sun_offset').css('transform','rotate3d(0,0,1,-30deg)');
					},1000);
				});
			},5000);

			zastavka_shown = true;
		
		}else if(h>=18 && h<21){
	
			$('.zastavka.day, .zastavka.evening').css('display','block');
			$('.zastavka.day .sun').css('display','none');
		
			$('.zastavka.day').css('z-index','99990');
			$('.zastavka.day .name').remove();
		
			setTimeout(function(){
				$('.zastavka.day').css('opacity',0);
			},500);

			setTimeout(function(){
				$('.moon_offset').css('transform','rotate3d(0,0,1,0deg)');
			},1000);

			setTimeout(function(){
				$('.zastavka').fadeOut(300,'linear',function() {
					setTimeout(function(){
						$('.sun_offset').css('transform','rotate3d(0,0,1,-30deg)');
					},1000);
				});
			},5000);

			zastavka_shown = true;
		
		}else if( (h>=21 && h<24) || (h>=0 && h<9) ) {
	
			$('.zastavka.evening, .zastavka.night').css('display','block');
			$('.zastavka.evening .moon_offset').remove();
			
			$('.zastavka.evening').css('z-index','99990');
			$('.zastavka.evening .name').remove();
			
			setTimeout(function(){
				$('.zastavka.evening').css('opacity',0);
			},500);

			setTimeout(function(){
				$('.zastavka.night .moon_offset').css('transform','rotate3d(0,0,1,0deg)');
			},1000);

			setTimeout(function(){
				$('.zastavka').fadeOut(300,'linear',function() {
					setTimeout(function(){
						$('.sun_offset').css('transform','rotate3d(0,0,1,-30deg)');
					},1000);
				});
			},5000);

			zastavka_shown = true;
			
		}else if( h>=9 && h<12 ) {

			$('.zastavka.night, .zastavka.morning').css('display','block');
			$('.zastavka.night .moon_offset').css('transform','rotate3d(0,0,1,0deg)');
			
			$('.zastavka.night').css('z-index','99990');
			$('.zastavka.night .name').remove();
			
			setTimeout(function(){
				$('.zastavka.night').css('opacity',0);
			},500);

			setTimeout(function(){
				$('.zastavka').fadeOut(300,'linear',function() {
					setTimeout(function(){
						$('.sun_offset').css('transform','rotate3d(0,0,1,-30deg)');
					},1000);
				});
			},5000);

			zastavka_shown = true;
		}
	}	
}