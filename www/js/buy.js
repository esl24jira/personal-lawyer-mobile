var familyOption = false;

myApp.onPageInit('buy', function (page) {
	showStatusBar();
	
	if( localStorage.getItem("products") )
	{
		var products = JSON.parse( localStorage.getItem("products") );
	
		$(products).each(function(index, element) {
			
			var currentTemplate = $('.swiper-container.buy .swiper-slide.template').clone();
			
			if(element.currency_sign == "KZT"){
				var currency = '₸';

				$(currentTemplate).find('.family').css('display','none');
			}else if(element.currency_sign == "RUB"){
				var currency = '₽';
			}

			$(currentTemplate).find('.top img').attr('src', element.image );
			$(currentTemplate).find('.top .caption').html( element.title );
			$(currentTemplate).find('.top .price').html( '<span>' + element.price + '</span> ' + currency + ' ' +  element.price_note );
			$(currentTemplate).find('.top .price').attr('data-price', element.price);
			$(currentTemplate).find('.top .price').attr('data-family-price', element.family_price);
			
			$(currentTemplate).attr('data-package-id', element.id);

			if(element.family_price == null || element.family_price == element.price){
				$(currentTemplate).find('.family').remove();
			}
			
			$(element.services).each(function(index2, element2) {
				if( element2.description == null ){
					$(currentTemplate).find('.productServices').append('<div class="title">' + element2.title + '</div><div class="description">' + element2.count + '</div>');
				}else{
					$(currentTemplate).find('.productServices').append('<div class="title">' + element2.title + '<img src="img/info-button (1).svg" data-description="' + element2.description + '"></div><div class="description">' + element2.count + '</div>');
				}				
			});
			
			$(currentTemplate).find('input[type=checkbox]:not(.promo input[type=checkbox])').on('change',function(){
				if( $(this).is(':checked') ){
					$(this).parent().parent().parent().find('.price span').text( $(this).parent().parent().parent().find('.price').attr('data-family-price') );
					familyOption = true;
				}else{
					$(this).parent().parent().parent().find('.price span').text( $(this).parent().parent().parent().find('.price').attr('data-price') );
					familyOption = false;
				}
			});

			var $promoValueEl = $(currentTemplate).find('.promo .promo__value');
			$(currentTemplate).find('.promo input[type=checkbox]').on('change', function(e) {
				var target = e.target;
				if (target.checked) {
					var prompt = myApp.prompt(
						'Введите ваш промо код',
						'Промо-код',
						function(val) {								
							var selectedProduct = $(e.target).parents('.swiper-slide');							
							$.ajax({
								method: "GET",
								url: 'https://lk.urist.els24.com/api/check-promocode',
								data: {
									promo_code: val,
									id: selectedProduct.data('package-id')
								}
							})
							.done(function(res) {
								console.log(res);								
								if (res.status === 'success') {																		
									selectedProduct.find('.price').attr('data-price', res.new_price).children('span').text(res.new_price);
									target.disabled = true;
									return myApp.alert('Ваш промо-код принят', 'Промо-код');
								}
								e.target.checked = false
								myApp.alert('Промо-код не действителен', 'Промо-код');
							})						
							.fail(function() {
								myApp.alert('Ошибка сети, повторите позже', 'Промо-код');
							})															
						},
						function() {
							e.target.checked = false
						}
					);
					console.log(prompt);
				}

				$promoValueEl.val('')				 
			})
			
			$(currentTemplate).find('img').not('.card-image').on('click',function(){
				$('.popover.popover-card-services-description .content-block').text( $(this).attr('data-description') );
				var clickedLink = this;
				myApp.popover('.popover-card-services-description', clickedLink );
			});
			
			$(currentTemplate).removeClass('template');
			$(currentTemplate).appendTo('.swiper-container.buy .swiper-wrapper');
		});

		var productsSwiper = myApp.swiper('.swiper-container.buy', {
			pagination:'.swiper-pagination.buy',
			paginationClickable: true,
			on: {
				slideChange: function () {
					console.log('444');
					console.log(this.activeIndex);
				}
			}
		});
		
		$('.page[data-page=buy] .button.big').on('click',function(){
			var $slected_slide = $('.swiper-container.buy .swiper-slide:nth-of-type(' + (productsSwiper.activeIndex + 2) + ')')
			var promo_code = $slected_slide.find('.promo .promo__value').val()
			selected_product = $slected_slide.attr('data-package-id');
			

			if(buy_mode == 1){
				mainView.router.loadPage({
					url: 'anketa.html',
					animatePages: true
				});
			}else if(buy_mode == 2){
				
				if( localStorage.getItem("mb") == 1){
					var buttons = [
					{
						text: 'Выберите тип оплаты',
						label: true
					},
					{
						text: 'Корпоративной картой',
						onClick: function(){	
							$.ajax({
								method: "POST",
								url: cordovaMode?"https://lk.urist.els24.com/api/mb-order":"echo_order.php",
								timeout: 10000,
								data: { 
									'token': localStorage.getItem('token'),
									'id': selected_product,
									'promo_code': promo_code
									//'family_option': ( familyOption?1:0 )
								},
								beforeSend: function() {
									showLoading();
								}
							})
							.done(function( msg ) {
								console.log( msg );
								if( msg.status == "success" && msg.paymentUrl != null )
								{
									myApp.alert('Сейчас Вы будете направлены на страницу оплаты', 'Оплата покупки', function () {
										var ref = cordova.InAppBrowser.open( msg.paymentUrl, '_blank', 'closebuttoncaption=Готово');

										 ref.addEventListener('exit', function(){
											 mainView.router.loadPage({
												url: 'cards.html',
												animatePages: true
											});
										 });
									});
								}
								else{
									myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
								}
							})
							.fail(function() {
								myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
							})
							.always(function() {
								hideLoading();
							});
						}
					},
					{
						text: 'Выставить счет',
						onClick: function(){							
							$.ajax({
								method: "POST",
								url: cordovaMode?"https://lk.urist.els24.com/api/mb-order-invoice":"echo_order.php",
								timeout: 10000,
								data: { 
									'token': localStorage.getItem('token'),
									'id': selected_product,
									'promo_code': promo_code
									//'family_option': ( familyOption?1:0 )
								},
								beforeSend: function() {
									showLoading();
								}
							})
							.done(function( msg ) {
								console.log( msg );
								if( msg.status == "success" )
								{
									myApp.alert('Счет на оплату сформирован и будет отправлен Вам на email', 'Спасибо', function () {
										mainView.router.loadPage({
											url: 'cards.html',
											animatePages: true
										});
									});
								}
								else{
									myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
								}
							})
							.fail(function() {
								myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
							})
							.always(function() {
								hideLoading();
							});
						}
					},
					{
						text: 'Отмена',
						color: 'red'
					}
				];

				myApp.actions(buttons);
				}else{					
					$.ajax({
						method: "POST",
						url: cordovaMode?"https://lk.urist.els24.com/api/order":"echo_order.php",
						timeout: 30000,
						data: { 
							'token': localStorage.getItem('token'),
							'id': selected_product,
							'family_option': ( familyOption?1:0 ),
							'promo_code': promo_code
						},
						beforeSend: function() {
							showLoading();
						}
					})
					.done(function( msg ) {
						console.log( msg );
						if( msg.status == "success" && msg.paymentUrl != null )
						{
							myApp.alert('Сейчас Вы будете направлены на страницу оплаты', 'Оплата покупки', function () {
								var ref = cordova.InAppBrowser.open( msg.paymentUrl, '_blank', 'closebuttoncaption=Готово');

								 ref.addEventListener('exit', function(){
									 mainView.router.loadPage({
										url: 'cards.html',
										animatePages: true
									});
								 });
							});
						}
						else{
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						}
					})
					.fail(function() {
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					})
					.always(function() {
						hideLoading();
					});
				}	
			}
		});
	}
});