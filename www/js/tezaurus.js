var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB
var hasTezaurus = localStorage.getItem('tezaurus');
var user_country = localStorage.getItem('country') === 'KZ' ? 3 : 1
var selectedTezaurus;
var dbTezaurus;


function getTezaurus(version) {
  return new Promise(function(resolve, reject) {        
    var requestDB = indexedDB.open("tezaurus", version);

    requestDB.onupgradeneeded = function (e) {
      console.log('[NEED_UPGRADE_DB]')
      var db = e.target.result;
      var trn = e.target.transaction;

      if (e.oldVersion === 0) {
        hasTezaurus = false;

        db.createObjectStore('categories', {
          keyPath: 'id'
        })

        db.createObjectStore('questions', {
          keyPath: 'id'
        })
        
        trn
          .objectStore('questions')
          .createIndex('title', 'title')
      }

      //For users which already have db
      if (e.oldVersion === 1) {
        trn
          .objectStore('questions')
          .createIndex('title', 'title')
      }
    }

    requestDB.onsuccess = function (e) {
      console.log('[SUCCESS_CONNECT_DB]')
      dbTezaurus = e.target.result;
      if (!hasTezaurus) {
        loadTezaurus(dbTezaurus)
          .then(function () {
            resolve(dbTezaurus)
          })
          .catch(function () {
            myApp.alert('Для работы этого раздела требуется доступ к Интернет', 'Нет соединения с интернетом')
          })
      } else {
        resolve(dbTezaurus)
      }
    }

    requestDB.onerror = function (e) {
      console.log('[ERROR_CONNECT_DB]', e)
      reject(e)
    }
  })
}

function loadTezaurus(db) {
  return new Promise(function(resolve) {    
    var tezaurusIsLoading = true;
    $.ajax({
      method: "POST",
      url: cordovaMode ?
        "https://cp.els24.com/api/v1?a=tzGetBase&country_id=" + user_country + "&language_id=1" : "echo_tezaurus.php",
      timeout: 300000,
      beforeSend: function () {
        $('.tezCategories').html('Загрузка раздела. Первый раз это может занять несколько минут. Пожалуйста, подождите.');
      }
    })
    .done(function (res) {
      var objectStoreCategories = db.transaction('categories', 'readwrite').objectStore('categories')
      var objectStoreQuestions = db.transaction('questions', 'readwrite').objectStore('questions')

      res.questions.forEach(function (el) { objectStoreQuestions.add(el) })
      res.categories.forEach(function (el) { objectStoreCategories.add(el) })
      
      localStorage.setItem('tezaurus', res.version)

      resolve(res)
    })
    .fail(function() {
      myApp.alert('Для работы с этим разделом требуется доступ к Интернет', 'Нет соединения с интернетом');
    })    
  })
}

initTezaurusInterface.initToggleAnswers = function() {
  $('.js-tezaurus-delegator').on('click', function(e) {    
    var $target = $(e.target);    
    if ($target.hasClass('question')) {      
      var id = $target.data('id')
      var objectStore = dbTezaurus.transaction('questions').objectStore('questions');
      var answer = objectStore.get(id.toString());

      selectedTezaurus = id;

      answer.onsuccess = function() {
        $('.popup-tezaurus .caption').html(answer.result.title);
        $('.popup-tezaurus .answer').html(answer.result.content);        
        myApp.popup('.popup-tezaurus');
        $('.popup-tezaurus .holder').scrollTop(0);	    
      }            
    }
  })
}

initTezaurusInterface.renderCategories = function () {  
  var objectStoreCategories = dbTezaurus.transaction("categories").objectStore("categories")
  var cursor = objectStoreCategories.openCursor()
  var innerHtml = ''

  cursor.onsuccess = function (e) {
    var cursor = e.target.result;
    if (cursor) {
      innerHtml += (
        '<div class="item" data-id="' + cursor.key + '"><div class="js-category"><img src="img/tezaurus/' + cursor.key + '.svg"><div class="title">' + cursor.value.title + '</div></div><div style="display:none;" class="questions"></div></div>'
      )
      return cursor.continue()
    }
    console.log(innerHtml);
    $('.tezCategories').html(innerHtml);      
  }  
}

initTezaurusInterface.initToggleQuestions = function () {
    $('.js-tezaurus-delegator').on('click', function (e) {                               
      var $target = $(e.target);
      if (!$target.hasClass('js-category')) {
        return;
      }
      
      var $item = $target.parent()
      var categoryID = $item.data('id')
      var $contentQuestions = $item.find('.questions')
      var $allQuestions = $(".tezCategories").find('.questions')          
      var alreadyOpened = $contentQuestions.css('display') === 'block';      

      if (!$contentQuestions.children().length) {                
        var cursor = dbTezaurus.transaction('questions').objectStore('questions').openCursor();
        var innerHtml = '';

        showLoading()
        
        cursor.onsuccess = function (e) {
          var cursor = e.target.result          
          if (cursor) {                           
            if (Number(cursor.value.category_id) === categoryID) {
              innerHtml += '<div class="question" data-id="' + cursor.key + '">' + cursor.value.title + '</div>'
            }
            return cursor.continue()
          }          

          $contentQuestions.html(innerHtml)                

          hideLoading()
          $allQuestions.hide()
          $contentQuestions.fadeIn(300)   
        }        
      } 

      $allQuestions.hide()
      if (!alreadyOpened) {
        return $contentQuestions.fadeIn(300)
      }       
    })
}

initTezaurusInterface.initPopups = function () {  
  $('.popup-tezaurus .close').on('click', function(e) {
    myApp.confirm('Отправить ответ на email?', 'Внимание', function(){
      $.ajax({
        method: 'POST',
        url: cordovaMode ? 
          "https://lk.urist.els24.com/api/thesaurus-send-answer" : 
          "echo_tezaurus_email.php",
        data: {
          token: localStorage.getItem('token'),
          id: selectedTezaurus
        },
        timeout: 10000,
        beforeSend: function() {
          showLoading();
        }        
      })
      .done(function(res) {
        if (res.status == "success") {
          myApp.alert(res.message, 'Спасибо', function () {
            myApp.closeModal('.popup-tezaurus');
          });
        } else {
          myApp.alert(res.message, 'Ошибка сервера');
        }
      })
      .fail(function() {        
        myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом');        
      })      
      .always(function() {
        hideLoading();
      })      
    })    
  });

  $('.popup-tezaurus .exit').on('click', function (event) {
    myApp.closeModal('.popup-tezaurus');
  });
}

function findByTitle(title) {  
  console.log(title)
  return new Promise(function(resolve, reject) {
    if (title === '') {
      resolve([])
    }

    if (title.length < 3) {
      return;
    }

    const trans = dbTezaurus.transaction("questions")
    const index = trans.objectStore("questions").index("title")
    const cursorReq = index.openCursor()
    const matches = []
    const reg = new RegExp(title, 'gi');

    cursorReq.onsuccess = function(e) {
      const cursor = e.target.result;
      if (cursor) {      
        if (cursor.key.match(reg)) {
          matches.push(cursor.value)
        }        
        return cursor.continue()
      } 

      return resolve(matches)
    }        
  })
  .then(function(res) {      
    let innerHtml = res.reduce(function (acc, next) {
      console.log(next)
      return acc += '<div class="question" data-id="' + next.id + '">' + next.title + '</div>';
    }, '')

    $(".tezSearch__results").html(innerHtml);
  })  
}

initTezaurusInterface.initSearch = function () {
  const findRes = debounce(findByTitle, 300)  
  $('.tezSearch__input input').on('keydown', function (e) {        
    //on Next tik for update value after keyUp event     
    setTimeout(function () { findRes(e.target.value.trim()) })
  })

  $('.tezSearch').show();
}

function initTezaurusInterface() {
  $('.tab-link.tezaurus').on('click', function (event) {
    getTezaurus(2)
      .then(function (db) {
        initTezaurusInterface.renderCategories()              
        initTezaurusInterface.initToggleQuestions()
        initTezaurusInterface.initToggleAnswers()          
        initTezaurusInterface.initPopups()
        initTezaurusInterface.initSearch()
      })        
  })  
}

$$(document).on('page:init', '.page[data-page="study"]', function () {
  initTezaurusInterface()
})

//UTILS 
function debounce(f, ms) {
  var timer = null;  
  return function() {        
    var args = arguments;
    if (timer) {
      clearTimeout(timer)
    }
    
    timer = setTimeout(function () {
      f.apply(null, args)      
      timer = null;
    }, ms)
  }
}
