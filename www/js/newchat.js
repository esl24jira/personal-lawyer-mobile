var myMessages;
var myMessagebar;
var message_sent = false;
var messageText = '';
var fileName = '';
var appTokent = localStorage.getItem('token');

myApp.onPageInit('chat', function (page) {
	console.log('[CURRENT CONSULTATION]', current_consultation);
	myMessages = myApp.messages('[data-page="chat"] .js-chat-messages', {
		autoLayout: true
	});

	renderComments(current_consultation.comments);
	initInterface();
})

function renderComments(comments) {
	comments.forEach((cmnt) => {
		var innerHTML = cmnt.comment || ''

		cmnt.files.forEach((file) => {
			console.log(file.name)
			var file_ext = file.name.match(/\.(.*)$/i) ? RegExp.$1 : '';
			console.log(file_ext);
			if (!file_ext) {
				return;
			}

			if (file_ext.match(/[jpg|jpeg|png|gif|bmp]$/i)) {
				if (cmnt.is_company) {
					return innerHTML += '<div><img src="https://lk.urist.els24.com/api/file?token=' + appTokent + '&id=' + file.id + ')</div>';
				}
				return innerHTML += '<img data-extension="' + file_ext + '" src="https://lk.urist.els24.com/api/file?token=' + appTokent + '&id=' + file.id + '">';
			}

			if (file_ext.match(/[mp3]$/i)) {
				if (cmnt.is_company) {
					return innerHTML += '<div class="js-audio-player"><img class="audio_player js-audio-play" src="img/play-button.svg"><img class="audio_player js-audio-pause" style="display: none" src="img/pause.svg"><img class="audio_player js-audio-rewind" src="img/rewind.svg"><audio class="js-audio-src" data-play="false" preload="auto"><source src="https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + file.id + '" type="audio/mp3"></audio></div>';
				}
			}

			if (cmnt.is_company) {
				return innerHTML += '<div class="fileclick"><img class="clip" data-id="' + file.id + '" data-extension="' + file_ext + '" src="img/Icons/chat-attach-white.svg"><p>' + file.original_name + '</p></div>';
			}
			innerHTML += '<div><img class="clip" data-id="' + file.id + '" data-extension="' + file_ext + '" src="img/Icons/chat-attach-white.svg"></div>';
		})

		var newMessage = {
			text: innerHTML,
			type: cmnt.is_company == 1 ? 'received' : 'sent',
			name: cmnt.is_company == 1 ? 'Личный Юрист' : null,
			date: cmnt.created_at,
		}

		if (!cmnt.is_company) {
			newMessage.label = cmnt.status_id == 2 ? 'Прочитано' : 'Доставлено'
		}

		myMessages.prependMessage(newMessage);
	})
}

function initInterface() {
	console.log('INIT CLICKS ON AUDIO FILES')
	initFileClicks.clickOnAudio()
}

initInterface.clickOnAudio = function () {	
	$('.js-audio-player').each(function () {
		var $target = $(this)
		var src = $target.find('.js-audio-src').get(0)
		var $rewindBtn = $target.find('.js-audio-rewind')
		var $pauseBtn = $target.find('.js-audio-pause')
		var $playBtn = $target.find('.js-audio-play')

		src.addEventListener('pause', function () {
			$pauseBtn.toggle();
			$playBtn.toggle();
		})

		src.addEventListener('play', function () {
			$pauseBtn.toggle();
			$playBtn.toggle();
		})

		$pauseBtn.on('click', () => src.pause())
		$playBtn.on('click', () => src.play())
		$rewindBtn.on('click', () => {
			src.currentTime = 0;
			src.pause();
		})

		src.addEventListener('canplay', function () {
			console.log('canplay')
			$target.addClass('active')
		})
	})
}

initInterface.clickOnImage = function () {
	$('.message .message-text .fileclick').not('.audio_player').on('click', function () {
		if ($(this).find('img').attr('data-extension').match(/[jpeg|jpg|bmp|gif]$/i)) {
			if (myApp.device.android) {
				PhotoViewer.show($(this).find('img').attr('src'));
			}
		} else {
			var file_id = $(this).find('img').attr('data-id');

			myApp.confirm('Отправить документ на email?', 'Отправка файла',
				function () {
					// url: cordovaMode ? "https://lk.urist.els24.com/api/send-file" : "echo_send_file.php",
					// data: {
					// 	'token': localStorage.getItem('token'),
					// 	'file_id': file_id
					// },								
				});
		}
	})
}

initInterface.initAndroidKeyboardHide = function() {
	if (cordovaMode && myApp.device.android) {
		$('[data-page="chat"] .page-content').on('click',function(){
			NativeKeyboard.hideMessengerKeyboard();
		});				

		//SOMETHING WITH TOOLBAR
		// $('#commentMessage').on('input propertychange keyup', function (event) {
		// 	$('.round').css('margin-bottom', ($('.toolbar.messagebar').height() + 25) + 'px');
		// });		

	}
}

initInterface.initChatAttachment = function() {
	$('#chatAttachment').on('change', function () {
		var file_name = $('#chatAttachment').val();

		if (file_name.indexOf('/') >= 0) {
			file_name = file_name.split('/');
		} else {
			file_name = file_name.split('\\');
		}

		file_name = file_name[file_name.length - 1];

		if (myApp.device.android) {
			myMessagebar.value(myMessagebar.value() + ' Прикреплен файл: ' + file_name);
		} else {
			NativeKeyboard.updateMessenger({
				text: messageText + ' Прикреплен файл: ' + file_name
			});
		}
	});
}

initInterface.getPermissions = function() {
	var permissions = cordova.plugins.permissions;

	permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function(status){
		if (!status.hasPermission) {
			permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE);
		}			
	})
}