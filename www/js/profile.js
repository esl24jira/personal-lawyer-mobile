var profile;

function setProfile()
{
	if( localStorage.getItem("profile") )
	{
		profile = JSON.parse( localStorage.getItem("profile") );
		
		if( localStorage.getItem("mb") == 0){
			$('.fiz').css('display','block');
			$('.mb').css('display','none');
			
			$('.page[data-page=profile] input[name=surname]').val( profile.last_name );
			$('.page[data-page=profile] input[name=name]').val( profile.first_name );
			$('.page[data-page=profile] input[name=secname]').val( profile.second_name );
			$('.page[data-page=profile] input[name=dob]').val( profile.birthday );
			$('.page[data-page=profile] input[name=city]').val( profile.city );
			$('.page[data-page=profile] input[name=email]').val( profile.email );
		}else{
			$('.fiz').css('display','none');
			$('.mb').css('display','block');
			
			$('.page[data-page=profile] input[name=surname]').val( profile.last_name );
			$('.page[data-page=profile] input[name=name]').val( profile.first_name );
			$('.page[data-page=profile] input[name=secname]').val( profile.second_name );
			$('.page[data-page=profile] input[name=inn]').val( profile.inn );
			$('.page[data-page=profile] input[name=bik]').val( profile.bik );
			$('.page[data-page=profile] input[name=kpp]').val( profile.kpp );
			$('.page[data-page=profile] input[name=address]').val( profile.address );
			$('.page[data-page=profile] input[name=bank]').val( profile.bank );
			$('.page[data-page=profile] input[name=bank_account]').val( profile.bank_account );
			$('.page[data-page=profile] input[name=organization]').val( profile.organization );
			$('.page[data-page=profile] input[name=email]').val( profile.email );
		}
	}
	
	if( localStorage.getItem('touch') && localStorage.getItem('touch') == 'true' ){
		$('#touchIdSwitch').prop('checked', true);
		$('#touchIdSwitch').parent().parent().parent().find('p').text('Настроен');
	}
/*
	if( localStorage.getItem('auth') && localStorage.getItem('auth') == 'pin' ){
		$('#pinSwitch').prop('checked', true);
		$('#pinSwitch').parent().parent().parent().find('p').text('Настроен');
	}
*/
}

function profileRequest()
{
	if( localStorage.getItem("mb") == 0){
		var targetUrl = 'https://lk.urist.els24.com/api/profile';
	}else{
		var targetUrl = 'https://lk.urist.els24.com/api/mb-profile';
	}
	
	$.ajax({
		method: "GET",
		url: cordovaMode?targetUrl:"echo_profile.php",
		data: { 
			'token': localStorage.getItem('token')
		},
		timeout: 10000,
		beforeSend: function() {
			
		}
	})
	.done(function( msg ) {
		console.log( msg );
		if( msg.status == "success" )
		{
			localStorage.setItem("profile", JSON.stringify(msg.data));
		}
	})
	.fail(function() {
		console.log( "profile error" );
	})
	.always(function() {
		//setProfile();
	});
}

myApp.onPageBeforeAnimation('profile', function (page) {
	profileRequest();
});

myApp.onPageInit('profile', function (page) {

	//$$('.popup-pin').off('popup:close');

	if(myApp.device.ios && window.screen.width == 375 && window.screen.height == 812){
	    $('#touchIdSwitch').parent().parent().parent().find('.title').text('Вход по Face ID');
	}
	
	$('.page[data-page=profile] input[name=password]').each(function(index, element) {
        $(element).on('focus',function(){
			$(element).parent().find('.title').addClass('active');
		});
		$(element).on('focusout',function(){
			if( $(element).val().length == 0 )
			{
				$(element).parent().find('.title').removeClass('active');
			}
		});
    });
	
	setProfile();
	
	profileRequest();
	
	$('.navbar .exit').off('click');
	$('.navbar .exit').on('click',function(){
		myApp.confirm('Вы действительно хотите выйти?', 'Выход', 
			function () {
				/*
				localStorage.setItem('token',null);
				localStorage.removeItem('cards');
				localStorage.removeItem('consultations');
				localStorage.removeItem('users');
				localStorage.removeItem('products');
				localStorage.removeItem('mb');
				localStorage.removeItem('pin');
				localStorage.removeItem('touch');
				localStorage.removeItem('pinRequested');
				*/
				var saved_current_version = localStorage.getItem('current_version');

				localStorage.clear();
				localStorage.setItem('tour',true);
				localStorage.setItem('current_version',saved_current_version);

				mainView.router.loadPage('login.html');
			
				hasActivatedCard = false;

				if (cordovaMode && window.plugins) {
					window.plugins.touchid.delete("MyKey", function() {});
				}

				$('.touchLogin').css('opacity', 0);
			},
			function () {
			}
		);
	});
	
	//var ptrContent = $$('.page[data-page=profile] .pull-to-refresh-content');
 /*
	ptrContent.on('ptr:refresh', function (e) {
		setTimeout(function () {
			profileRequest();
			myApp.pullToRefreshDone();
		}, 500);
	});
*/
	$('.page[data-page=profile] .wrapper .button.changePass').on('click',function(){
		//$('div.window.changePassword').removeClass('closed');
		$('div.window.changePassword').fadeIn();
		showBg();
	});
	
	$('.page[data-page=profile] .wrapper .button.changeEmail').on('click',function(){
		//$('div.window.changePassword').removeClass('closed');
		$('div.window.changeEmail').fadeIn();
		showBg();
	});
	
	$('.page[data-page=profile] #callType .radiooption').on('click',function(){
		$('.page[data-page=profile] #callType .radiooption').removeClass('active');
		$(this).addClass('active');
		$(this).parent().attr('data-selected-call-type', $(this).attr('data-call-type') );
		localStorage.setItem('callType', $(this).attr('data-call-type') );
	});
	
	$('#touchIdSwitch').on('change',function(){
		if( $(this).is(':checked') ){
			if (cordovaMode && window.plugins) {
				window.plugins.touchid.isAvailable(function() {
					window.plugins.touchid.has("MyKey", function() {
						/*
						alert("Touch ID avaialble and Password key available");
						window.plugins.touchid.verify("MyKey", "Вход по отпечатку пальца", function(password) {
							alert("Tocuh " + password);
						});
						*/
					}, function() {
						//alert("Touch ID available but no Password Key available");
						myApp.confirm('Установить вход по ' + auth_type + '?', 'Подтвердите', 
						function () {
							window.plugins.touchid.save("MyKey", "MyPassword", function() {
								
								localStorage.setItem('touch','true');
								$('#touchIdSwitch').parent().parent().parent().find('p').text('Настроен');
								$('#pinSwitch').prop('checked', false);
								$('#pinSwitch').parent().parent().parent().find('p').text('Выключен');

								myApp.alert('Вход по ' + auth_type + ' настроен.', 'Спасибо', function () {
									
								});

								$('.callbutton.touchLogin').css('opacity',1);
							});
						},
						function(){
							$('#touchIdSwitch').prop('checked', false);
						}			 
						);
					});
				}, function(msg) {
					myApp.alert('Вход по ' + auth_type + ' не настроен.', 'Ошибка', function () {});
					$('#touchIdSwitch').prop('checked', false);
				});
			}
		}else{
			localStorage.setItem('touch','false');
			window.plugins.touchid.delete("MyKey", function() {
				myApp.alert('Вход по ' + auth_type + ' выключен.', 'Спасибо', function () {});
				$('#touchIdSwitch').parent().parent().parent().find('p').text('Выключен');
				$('.callbutton.touchLogin').css('opacity',0);
			});
		}
	});
	
	$('#pinSwitch').on('change',function(){
		if( $(this).is(':checked') ){
			$('#touchIdSwitch').prop('checked', false);
			$('#touchIdSwitch').parent().parent().parent().find('p').text('Выключен');
			
			//localStorage.setItem('auth','pin');
			changingPin=true;
			entered_pin='';
			$('.popup-pin .pindot').removeClass('active');
			myApp.popup('.popup.popup-pin');
			
		}else{
			localStorage.removeItem('auth');
			$('#pinSwitch').parent().parent().parent().find('p').text('Выключен');
		}
	});
	
	$('.reply').on('click',function(){
		$('.reply').removeClass('checked');
		$(this).addClass('checked');
	});
	
	$('.reply.email').text('По почте (' + profile.email + ')');
	$('.reply.phone').text('По телефону (' + profile.phone + ')');
	
	$('#helptext').on('focus',function(){
		$(this).select();
		/*
		if(myApp.device.android){
			mainView.hideToolbar(false);
			$('.button.big.orange').css('display','none');
		}
		*/
	});
	
	$('#helptext').on('focusout',function(){
		if(myApp.device.android){
			mainView.showToolbar(false);
			$('.button.big.orange').css('display','table');
		}
	});
	
	$('.button.big.orange').on('click',function(){
		if( $('#helptext').val().length>10 ){
		   $.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/support":"echo_support.php",
				timeout: 10000,
				data: { 
					last_name : profile.last_name,
					first_name : profile.first_name,
					second_name : profile.second_name,
					email : profile.email,
					phone : profile.phone,
					description : $('#helptext').val(),
					url: 'mobile'
				},
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" ){
					$('#helptext').val('');
				}
				else{
					myApp.alert(msg.message, 'Ошибка обработки запроса', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		}else{
			myApp.alert('Пожалуйста, введите текст сообщения', 'Ошибка', function () {
				$('#helptext').focus();
			});
		}
	});
});