var cordovaMode = false;
var premiumUser = false;
var hasActivatedCard = false;

var savedPhone;
var savedPass;

var changingPin = false;

var speaker_mode = 'ear';

var wrong_pin_count = 0;

var appHasLoadedLong = false;

var selected_consultation_to_get_consultation = null;
var selected_card_to_get_consultation = null;

var selected_card_for_user_to_add = null;

var tezaurusIsLoading = false;

var userFirstName = false;

var loginStarted = false;

var auth_type = 'отпечатку пальца';

// Initialize your app
var myApp = new Framework7({
	swipeBackPage: false,
	modalButtonCancel: 'Отмена',
	modalButtonOk: 'OK'
});

var userLoggedIn = false;

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

if(myApp.device.android) {
	$('head').append('<link rel="stylesheet" type="text/css" href="css/forAndroid.css" />');
}

$('.toolbar a.menu:not(.open-popup)').not('#popupCall').each(function(index, element) {
	$(element).on('click',function(){
		//check_auth();

		if( $(this).attr('href') != 'consultations.html'){
			$('#newyear').fadeOut(1);
		}
		
		if(cordovaMode){
			window.ga.trackEvent('Menu', $(element).find('p').text() );
		}
		
		$('.toolbar a.menu').each(function(index2, element2) {
			$(element2).find('img').attr('src', $(element2).attr('data-bg') );
		});
		
		$(element).find('img').attr('src', $(element).attr('data-active-bg') );
		
		$('.toolbar a.menu').removeClass('active');
		$(element).addClass('active');
		
		if(myApp.device.android){
			if( !$(element).hasClass('popupCall') ){
				if( $(element).hasClass('loadCards') ){
					$('.navbar').css('box-shadow','none');
				}else{
					$('.navbar').css('box-shadow', '0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12)');
				}
			}
		}
	});
});

var salt = "sbsalt";

var selected_consultation;
var current_consultation;
var selected_card;
var selected_chat;
var chat = [];
var consultations;
var cards;
var call_has_started = false;
var selected_product;
var buy_mode = 0;

var latest_click = new Date();

var zastavka_shown = false;

function loadStartPage(){
	//alert(localStorage.getItem('logoutdate'));

	if(!localStorage.getItem('current_version') || parseInt(localStorage.getItem('current_version')) < current_version ){
		localStorage.removeItem('pin');
		localStorage.removeItem('touch');
		localStorage.removeItem('pinRequested');

		if (cordovaMode && window.plugins) {
			window.plugins.touchid.delete("MyKey", function() {});
		}

		if(localStorage.getItem('current_version')){
			localStorage.clear();
			localStorage.setItem('tour', true);
		}else{
			localStorage.clear();
		}

		localStorage.setItem('current_version', current_version);

	}

	loginStarted = true;

	var winterStart = Date.parse('Dec 25, 2017');
	var winterEnd = Date.parse('Jan 15, 2018');
	var now = new Date()

	if( now >= winterStart && now <= winterEnd && !localStorage.getItem('winter_shown')){
		$('.winter').fadeIn(1, function() {});

		$('.winter .button').on('click', function(event) {
			$('.winter').fadeOut('300', function() {
				localStorage.setItem('winter_shown',true);
			});
		});
	}
	
	if( !localStorage.getItem('tour') )
	{
		mainView.router.loadPage({
			url: 'tour.html',
			animatePages: false
		});
	}
	else 
	{
		if( localStorage.getItem('pin') && localStorage.getItem('pin') == 'true' ){

			myApp.popup('.popup.popup-pin', false, false);

			if( localStorage.getItem('touch') && localStorage.getItem('touch') == 'true' ){	
				if (cordovaMode && window.plugins) {
					window.plugins.touchid.isAvailable(function() {
						window.plugins.touchid.has("MyKey", function() {
							//alert("Touch ID avaialble and Password key available");
							$('.callbutton.touchLogin').css('opacity',1);
							setTimeout( function(){
								$('.callbutton.touchLogin').click();
							},1000);
						}, function() {
							//alert("Touch ID available but no Password Key available");
							localStorage.removeItem('touch');
						});
					}, function(msg) {

					});
				}	
			}
/*
			$$('.popup-pin').on('popup:close', function () {
				if(loginStarted){
					mainView.router.loadPage({
						url: 'login.html',
						animatePages: false
					});
				}	
			});
*/
		}
		else
		{
			mainView.router.loadPage({
				url: 'login.html',
				animatePages: false
			});
		}	
	}
}

$('.window input').each(function(index, element) {
	$(element).on('focus',function(){
		$(element).parent().find('.title').addClass('active');
		$(element).addClass('active');
	});
	$(element).on('focusout',function(){
		if( $(element).val().length == 0 )
		{
			$(element).parent().find('.title').removeClass('active');
			$(element).removeClass('active');
		}
	});
});

function showBg(){
	//$('.bg').addClass('shown');
	//$('.bg').css('display','block');
	$('.bg').fadeIn();
}

function hideBg(){
	//$('.bg').removeClass('shown');
	//setTimeout(function() { $('.bg').css('display','none'); }, 300);
	$('.bg').fadeOut();
}

var change_password_clicked = false;

$('div.window.changePassword .button.left').on('click',function(){
	$('.window.changePassword input').each(function(index, element) {
		$(element).removeClass('error');
		$(element).parent().find('.hint').css('display','none');
	});
	
	if( $('.window.changePassword input[name=oldpass]').val().length < 1 )
	{
		$('.window.changePassword input[name=oldpass]').addClass('error');
		$('.window.changePassword input[name=oldpass]').focus();
		$('.window.changePassword input[name=oldpass]').parent().find('.hint').css('display','block');
	}
	else if( $('.window.changePassword input[name=newpass]').val().length < 6 )
	{
		$('.window.changePassword input[name=newpass]').addClass('error');
		$('.window.changePassword input[name=newpass]').focus();
		$('.window.changePassword input[name=newpass]').parent().find('.hint').css('display','block');
	}
	else if( $('.window.changePassword input[name=newpassagain]').val() != $('.window.changePassword input[name=newpass]').val() )
	{
		$('.window.changePassword input[name=newpassagain]').addClass('error');
		$('.window.changePassword input[name=newpassagain]').focus();
		$('.window.changePassword input[name=newpassagain]').parent().find('.hint').css('display','block');
	}
	else
	{
		if( !change_password_clicked )
		{
			$.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/change-password":"echo_password.php",
				data: { 
					'token': localStorage.getItem('token'),
					'old_password': md5( $('#oldpass').val() + salt ),
					'new_password': md5( $('#newpass').val() + salt )
				},
				timeout: 10000,
				beforeSend: function() {
					change_password_clicked = true;
					$('div.window.changePassword .button.left').css('opacity',0.5);
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" )
				{
					localStorage.setItem('token',null);
					$('div.window.changePassword').fadeOut();
					hideBg();
					
					myApp.alert('Пароль успешно изменен', 'Смена пароля', function () {});
					
					mainView.router.loadPage('login.html');
				}
				else if( msg.status == "failure" && msg.message == "invalid old password" )
				{
					myApp.alert('Старый пароль указан неверно. Пароль не изменен.', 'Ошибка', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				change_password_clicked = false;
				$('div.window.changePassword .button.left').css('opacity',1.0);
			});
		}
	}
});

$('div.window.changePassword .button.right').on('click',function(){
	$('div.window.changePassword').fadeOut();
	hideBg();
});

const $emailChangeContainer = $('.window.changeEmail')
const $emailInput = $emailChangeContainer.find('input[type=email]')
const $emailHint = $emailInput.next('.hint')

$emailChangeContainer.find('.button.left').on('click',function(){
	const regexp = /.+@.+\..{2}$/	
	$emailInput.removeClass('error')
	$emailHint.hide()

	if (!regexp.test($emailInput.val())) {		
		$emailInput.addClass('error').focus();
		$emailHint.show()
	}	else {		
		$.ajax({
			method: "POST",
			url: cordovaMode?"https://lk.urist.els24.com/api/profile":"echo_email.php",
			data: { 
				'token': localStorage.getItem('token'),
				'email': $emailInput.val()
			},
			timeout: 10000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {				
			if( msg.status == "success" ) {
				myApp.alert('Email успешно изменен', 'Смена email');
				$('.window.changeEmail, .bg').fadeOut();				
				$('.page[data-page=profile] input[name=email]').val($emailInput.val());								
				var profile = JSON.parse( localStorage.getItem("profile") );
				profile.email = $emailInput.val();
				localStorage.setItem("profile", profile);				
				$emailInput.removeClass('error').val('')
				$emailHint.hide()
			}
			else {
				myApp.alert('Ошибка смены email.', 'Ошибка');
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом');
		})
		.always(function() {
			hideLoading();			
		});
	}	
});

$('div.window.changeEmail .button.right').on('click',function(){
	$('div.window.changeEmail').fadeOut();		
	hideBg();	
	$emailInput.removeClass('error').val('')
	$emailHint.hide()
});

$('.window.blagodarnost .button.right').on('click',function(){
	hideBg();
	$('.window.blagodarnost').fadeOut();
});

$('.window.pretenziya .button.right').on('click',function(){
	hideBg();
	$('.window.pretenziya').fadeOut();
});

$('.window.blagodarnost .button.left').on('click',function(){
	$('.window.blagodarnost .hint').css('display','none');
	$('.window.blagodarnost textarea').removeClass('error');
	
	if($('.window.blagodarnost textarea').val() == 0){
		$('.window.blagodarnost textarea').focus();
		$('.window.blagodarnost textarea').parent().find('.hint').css('display','block');
		$('.window.blagodarnost textarea').addClass('error');
	}else{
		var formData = new FormData();
		formData.append('token', localStorage.getItem('token') );
		formData.append('card_id', current_consultation.card_id );
		formData.append('description', $('.window.blagodarnost textarea').val() );
		formData.append('services_id', 68 );
		formData.append('low_country_id', 1 );
		formData.append('related_task_id', current_consultation.id );
		formData.append('image', document.getElementById("blagodarnostImageHolder").files[0] ); 
		
		$.ajax({
			method: "POST",
			url: cordovaMode?"https://lk.urist.els24.com/api/consultation":"echo_create_consultation.php",
			data: formData,
			timeout: 10000,
			contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
			processData: false, // NEEDED, DON'T OMIT THIS
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if(msg.status == 'success'){
				$('.window.blagodarnost').fadeOut();
				
				myApp.alert('Благодарность добавлена', 'Спасибо', function () {});
				
				var text = $('.window.blagodarnost textarea').val()
				var event = new CustomEvent('blagodarnostAdded', { detail: text });
				window.dispatchEvent(event)

				$('.page[data-page=consultation] .blagodarnost').html( $('.window.blagodarnost textarea').val() );
				$('.window.blagodarnost textarea').val('');
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
			hideBg();
		});
	}
});

$('#blagodarnostImageHolder').on('change',function(){
	if( $(this).get(0).files.length>0 ){
		$(this).parent().find('.button').addClass('withfile');
	}else{
		$(this).parent().find('.button').removeClass('withfile');
	}
});

$('#pretenziyaImageHolder').on('change',function(){
	if( $(this).get(0).files.length>0 ){
		$(this).parent().find('.button').addClass('withfile');
	}else{
		$(this).parent().find('.button').removeClass('withfile');
	}
});

$('.window.pretenziya .button.left').on('click',function(){
	$('.window.pretenziya .hint').css('display','none');
	$('.window.pretenziya textarea').removeClass('error');
	
	if($('.window.pretenziya textarea').val() == 0){
		$('.window.pretenziya textarea').focus();
		$('.window.pretenziya textarea').parent().find('.hint').css('display','block');
		$('.window.pretenziya textarea').addClass('error');
	}else{
		var formData = new FormData();
		formData.append('token', localStorage.getItem('token') );
		formData.append('card_id', current_consultation.card_id );
		formData.append('description', $('.window.pretenziya textarea').val() );
		formData.append('services_id', 67 );
		formData.append('low_country_id', 1 );
		formData.append('related_task_id', current_consultation.id );
		formData.append('image', document.getElementById('pretenziyaImageHolder').files[0] ); 
		
		$.ajax({
			method: "POST",
			url: cordovaMode?"https://lk.urist.els24.com/api/consultation":"echo_create_consultation.php",
			data: formData,
			timeout: 10000,
			contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
			processData: false, // NEEDED, DON'T OMIT THIS
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if(msg.status == 'success'){
				$('.window.pretenziya').fadeOut();
				
				myApp.alert('Претензия добавлена', 'Спасибо');

				var text = $('.window.pretenziya textarea').val();
				var event = new CustomEvent('pretenziyaAdded', { detail: text });
				window.dispatchEvent(event)

				$('.page[data-page=consultation] .pretenziya').html( $('.window.pretenziya textarea').val() );
				$('.window.pretenziya textarea').val('');
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
			hideBg();
		});
	}
});


$('.selectmenu.sbcards .button').on('click',function(){
	$('.selectmenu.sbcards').removeClass('active');
	$('.bg').fadeOut();
});

function showLoading(){
	//$('.loadingScreen').css('display','table');
	$('.loadingScreen').fadeIn();
	$('.statusBarBg').addClass('shown');
	/*
	if( cordovaMode ) {
		StatusBar.hide();
	};
	*/
}

function hideLoading(){
	//$('.loadingScreen').css('display','none');
	$('.loadingScreen').fadeOut();
	$('.statusBarBg').removeClass('shown');
	/*
	if( cordovaMode ) {
		StatusBar.show();
	};
	*/
}

$('.menu.loadProfile').on('click',function(){
	if( localStorage.getItem("profile") )
	{
		mainView.router.loadPage({
			url: 'profile.html',
			animatePages: false
		});
	}
	else
	{
		if( localStorage.getItem("mb") == 0){
			var targetUrl = 'https://lk.urist.els24.com/api/profile';
		}else{
			var targetUrl = 'https://lk.urist.els24.com/api/mb-profile';
		}
		
		$.ajax({
			method: "GET",
			url: cordovaMode?targetUrl:"echo_profile.php",
			data: { 
				'token': localStorage.getItem('token')
			},
			timeout: 10000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.status == "success" )
			{
				localStorage.setItem("profile", JSON.stringify(msg.data));
				
				mainView.router.loadPage({
					url: 'profile.html',
					animatePages: false
				});
			}
			else if( msg.status == "failure" && msg.message == 'user not found' )
			{
				localStorage.setItem("token", null);
				
				mainView.router.loadPage({
					url: 'login.html',
					animatePages: true
				});
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	}
});

$('.menu.loadCards').on('click',function(){
	if( localStorage.getItem("cards") )
	{
		mainView.router.loadPage({
			url: 'cards.html',
			animatePages: false
		});
	}
	else
	{
		$.ajax({
			method: "GET",
			url: cordovaMode?"https://lk.urist.els24.com/api/cards":"echo_cards.php",
			data: { 
				'token': localStorage.getItem('token')
			},
			timeout: 5000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.status == "success" )
			{
				localStorage.setItem("cards", JSON.stringify(msg.data));
				
				mainView.router.loadPage({
					url: 'cards.html',
					animatePages: false
				});
			}
			else if( msg.status == "failure" && msg.message == 'user not found' )
			{
				localStorage.setItem("token", null);
				
				mainView.router.loadPage({
					url: 'login.html',
					animatePages: true
				});
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	}
});

$('.menu.loadStudy').on('click',function(){
	//if( localStorage.getItem("news") )
	if( true )
	{
		mainView.router.loadPage({
			url: 'study.html',
			animatePages: false
		});
	}
	else
	{
		$.ajax({
			method: "GET",
			url: cordovaMode?"https://lk.urist.els24.com/api/education":"echo_education.php",
			data: { 
				'token': localStorage.getItem('token')
			},
			timeout: 10000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.status == "success" )
			{
				localStorage.setItem("news", JSON.stringify(msg.data));
				
				mainView.router.loadPage({
					url: 'study.html',
					animatePages: false
				});
			}
			else if( msg.status == "failure" && msg.message == 'user not found' )
			{
				localStorage.setItem("token", null);
				
				mainView.router.loadPage({
					url: 'login.html',
					animatePages: true
				});
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	}
});

$('#connectToSipButtonDiv').on('click',function(){
	if(myApp.device.android){
		if(!call_has_started){
			startCall();
			earphone();
		}else{
			endCall();
		}
	}
});

$('.popup-call .caller.numbers .callbutton').on('touchstart',function(){
	$(this).addClass('active');
});

$('.popup-call .caller.numbers .callbutton').on('touchend',function(){
	$(this).removeClass('active');
});

$('.popup-call .caller .callbutton.dtmf').on('click',function(){
	dtmf( $(this).attr('data-num') );
	navigator.vibrate(100);
});

$('#earphone').on('click',function(){
	earphone();
});

$('#speaker').on('click',function(){
	speaker();
});

$('#voicetarget').on('click',function(){
	switch_voice_target();
});

function goBack(){
	if( $('.navbar .navbar-inner.navbar-on-center .back.link').length > 0 )
	{
		mainView.router.back();
		myApp.closeModal();
	}
}

function formatDate(input, type){
	var data = input.split(" ");
	
	if(type=='date'){
		data = data[0];
		data = data.split('-');
		var output = data[2] + '.' + data[1] + '.' + data[0];
	}
	
	if(type=='datetime'){
		var date = data[0];
		date = date.split('-');
		date = date[2] + '.' + date[1] + '.' + date[0];
		
		var time = data[1];
		time = time.split(':');
		time = time[0] + ':' + time[1];
		
		var output = date + ' ' + time;
	}
	return output;
}

function hideStatusBar(){
	//$('.statusbar-overlay').addClass('white');
	/*
	if(cordovaMode){
		StatusBar.hide();
	}
	*/
}

function showStatusBar(){
	//$('.statusbar-overlay').removeClass('white');
	/*
	if(cordovaMode){
		StatusBar.show();
	}
	*/
}

setTimeout(function(){
	appHasLoaded = true;
	console.log(appHasLoaded);
},1000);


$('.listview .closeListview').on('click',function(){
	$('.bg, .listview').fadeOut();
});

$('html').on('touchstart',function(e){
	latest_click = new Date();
});

var first_check = true;

var check_auth_timeout = setInterval(function(){
	check_auth();
},1000);

function check_auth(e){

	var now = new Date();
	var inactivetime = now - latest_click;

	var diffMins = Math.floor((inactivetime % 86400000) / 60000);

	//alert(diffMins);
	
	if(diffMins >= 5 && !loginStarted ){
		//e.preventDefault();
		//alert('Время сессии истекло');
		
		$('#pin_message').text('Введите ваш пин–код');
		$('.popup-pin .pindot').removeClass('active');
		$('.callbutton.backDelete').css('opacity', '0');
		entered_pin='';
		latest_click = new Date();
		first_check = false;
		//zastavka_shown = false;
		hideLoading();
		myApp.closeModal();

		localStorage.setItem("token", null);
		//loadStartPage();

		if( localStorage.getItem('pin') && localStorage.getItem('pin') == 'true' ){
			myApp.popup('.popup.popup-pin', false, false);
			$('.callbutton.touchLogin').click();
		}
	}
}

$('.popup-pin .callbutton').on('touchstart',function(){
	$(this).addClass('active');
});

$('.popup-pin .callbutton').on('touchend touchleave',function(){
	$(this).removeClass('active');
});

$('.popup-pin .callbutton').not('.touchLogin').on('click',function(){
	
	if( $(this).hasClass('backDelete') ){
		if(entered_pin.length>0){
			entered_pin = entered_pin.slice(0,-1);
		}

		if(entered_pin.length==0){
			$('.callbutton.backDelete').css('opacity', '0');
		}
	}
	else if( $(this).hasClass('allDelete') ){
		entered_pin = '';
	}
	else
	{
		entered_pin += parseInt( $(this).text() );

		$('.callbutton.backDelete').css('opacity', '1');
	}
		
	//console.log(entered_pin);

	$('.popup-pin .pindot').removeClass('active');
	for (i = 0; i < entered_pin.length; i++) { 
		$('.popup-pin .pindot:eq('+i+')').addClass('active');
	}

	if(entered_pin.length==4){
		
		$$('.popup-pin').on('popup:close',function(){});

		//устанока ПИН
		if(changingPin){
			$('#pin_message').text('Введите ваш пин–код');
			localStorage.setItem('userPin', md5(entered_pin) );
			myApp.closeModal('.popup.popup-pin');
			changingPin=false;
			localStorage.setItem('pin','true');

			myApp.alert('ПИН код успешно сохранен', 'Спасибо', function(){
				mainView.router.loadPage('consultations.html');
				
				setTimeout(function(){
					if(!localStorage.getItem('touchRequested') || localStorage.getItem('touchRequested') == null){
						if (cordovaMode && window.plugins) {
							window.plugins.touchid.isAvailable(
								function() {
									myApp.confirm('Установить вход по '+auth_type+'?', 'Внимание', 
										function () {
											window.plugins.touchid.isAvailable(function() {
												window.plugins.touchid.has("MyKey", function() {
													/*
													alert("Touch ID avaialble and Password key available");
													window.plugins.touchid.verify("MyKey", "Вход по отпечатку пальца", function(password) {
														alert("Tocuh " + password);
													});
													*/
												}, function() {
													//alert("Touch ID available but no Password Key available");
													window.plugins.touchid.save("MyKey", "MyPassword", function() {
															
														localStorage.setItem('touch','true');

														$('.callbutton.touchLogin').css('opacity',1);
														
														myApp.alert('Вход по '+auth_type+' настроен.', 'Спасибо', function () {
															
														});
													});
												});
											}, function(msg) {
												myApp.alert('Вход по '+auth_type+' не настроен.', 'Ошибка', function () {});
											});

											localStorage.setItem('touchRequested','true');
										},
										function(){
											localStorage.setItem('touchRequested','true');
										}
									);
								},
								function(msg) {
									//myApp.alert('Вход по отпечатку пальца не настроен.', 'Ошибка', function () {});
								}
							);
						}
					}
				},5000);
			});
		//авторизация		
		}else{
			if( localStorage.getItem('userPin') == md5(entered_pin) ){
				$('#pin_message').text('Введите ваш пин–код');
				$.ajax({
					method: "GET",
					url: cordovaMode?"https://lk.urist.els24.com/api/auth":"login_echo.php",
					data: { 
						phone: localStorage.getItem('phone'), 
						password: localStorage.getItem('pass')
					},
					timeout: 10000,
					beforeSend: function() {
						showLoading();
					}
				})
				.done(function( msg ) {
					console.log( msg );
					if( msg.status == "success" )
					{
						localStorage.setItem('token', msg.token);
						loginStarted = false;
						myApp.closeModal();
						
						userFirstName = msg.first_name;

						mainView.router.loadPage({
							url: 'consultations.html',
							animatePages: false
						});

						$('#mainToolbar').removeClass('toolbar-hidden');

						//zastavka_shown = false;
					}
					else
					{
						myApp.alert('Пожалуйста, введите пароль заново', 'Ошибка авторизации', function () {
							myApp.closeModal('.popup.popup-pin');

							localStorage.removeItem('pin');
							localStorage.removeItem('touch');
							localStorage.removeItem('pinRequested');

							$('.callbutton.touchLogin').css('opacity',0);

							window.plugins.touchid.delete("MyKey", function() {});
							
							mainView.router.loadPage({
								url: 'login.html',
								animatePages: false
							});
						});
					}
				})
				.fail(function() {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {
						/*
						mainView.router.loadPage({
							url: 'login.html',
							animatePages: false
						});
						*/

						$('.popup-pin .pindot').removeClass('active');
						entered_pin='';
						$('.callbutton.backDelete').css('opacity', '0');


					});
				})
				.always(function() {
					hideLoading();
				});
			}else{
				var casesCaptionTry = ['попытка', 'попытки', 'попыток']
				var leftTryings = 3 - wrong_pin_count
				$('#pin_message').text('Неверный пин. Осталось ' + leftTryings + " " + declOfNum(casesCaptionTry, leftTryings));
				entered_pin = '';
				$('.popup-pin .pindot').removeClass('active');

				$('.callbutton.backDelete').css('opacity', '0');
				
				wrong_pin_count++;
				
				if(wrong_pin_count>=4){
					myApp.alert('Вы ввели ПИН код неправильно 4 раза. Пожалуйста, введите пароль.', 'Ошибка авторизации', function () {
						myApp.closeModal('.popup.popup-pin');
						
						$('.zastavka').css('display','none');
						zastavka_shown=true;
						
						mainView.router.loadPage({
							url: 'login.html',
							animatePages: false
						});
						
						wrong_pin_count=0;
					});
				}
			}
		}
	}
});

if(myApp.device.ios && window.screen.width == 375 && window.screen.height == 812){
    auth_type = 'Face ID';
    $('.touchLogin').css('background-image', 'url(img/faceid.svg)');
}

//вход по отпечатку
$('.callbutton.touchLogin').on('click', function(event) {
	if (cordovaMode && window.plugins && $('.callbutton.touchLogin').css('opacity') != 0 ) {
		window.plugins.touchid.isAvailable(function() {
			window.plugins.touchid.has("MyKey", function() {
				//alert("Touch ID avaialble and Password key available");
				window.plugins.touchid.verify("MyKey", "Вход по отпечатку пальца", 
				function(password) {
					$.ajax({
						method: "GET",
						url: cordovaMode?"https://lk.urist.els24.com/api/auth":"login_echo.php",
						data: { 
							phone: localStorage.getItem('phone'), 
							password: localStorage.getItem('pass')
						},
						timeout: 10000,
						beforeSend: function() {
							showLoading();
						}
					})
					.done(function( msg ) {
						console.log( msg );
						if( msg.status == "success" )
						{
							localStorage.setItem('token', msg.token);
							loginStarted = false;

							userFirstName = msg.first_name;

							myApp.closeModal('.popup.popup-pin');
							
							mainView.router.loadPage({
								url: 'consultations.html',
								animatePages: false
							});
						}
						else
						{
							myApp.alert('Пожалуйста, введите пароль заново', 'Ошибка авторизации', function () {
								myApp.closeModal('.popup.popup-pin');

								localStorage.removeItem('pin');
								localStorage.removeItem('touch');
								localStorage.removeItem('pinRequested');

								window.plugins.touchid.delete("MyKey", function() {});

								$('.callbutton.touchLogin').css('opacity',0);
								
								mainView.router.loadPage({
									url: 'login.html',
									animatePages: false
								});
							});
						}
					})
					.fail(function() {
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {
							/*
							mainView.router.loadPage({
								url: 'login.html',
								animatePages: false
							});
							*/
						});
					})
					.always(function() {
						hideLoading();
					});
				},
				function(){
					/*
					mainView.router.loadPage({
						url: 'login.html',
						animatePages: false
					});
					*/
				});
			}, function() {
				//alert("Touch ID available but no Password Key available");
			});
		}, function(msg) {
			myApp.alert('Вход по '+auth_type+' недоступен на устройстве.', 'Ошибка', function () {
				$('.callbutton.touchLogin').css('opacity',0);
				//$('.callbutton.touchLogin').off('click');
				localStorage.setItem('touch',false);
			});
		});
	}
});

$.ajaxSetup({ cache: false });

function onResume() {
    check_auth();
    //loadStartPage();
/*
    if( localStorage.getItem('pin') && localStorage.getItem('pin') == 'true' ){
		myApp.popup('.popup.popup-pin', false, false);
	}
*/
}

$('#pin_reset').on('click', function(event) {
	myApp.confirm('Сбросить ПИН-код?', 'Внимание', function () {
		myApp.closeModal();

		localStorage.removeItem('pin');
		localStorage.removeItem('touch');
		localStorage.removeItem('pinRequested');

		if(cordovaMode){
			window.plugins.touchid.delete("MyKey", function() {});
		}

		$('.callbutton.touchLogin').css('opacity',0);

		mainView.router.loadPage({
			url: 'login.html',
			animatePages: true
		});

		myApp.alert('Пожалуйста, введите номер телефона и пароль', 'Внимание', function () {});
	});
});