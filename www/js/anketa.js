var ref;
var dateOk = false;
var selectedBirthdate = null;

function formatDobDate(input){
	var data = input.split(' ');
	data = data[0] + ' ' + data[1] + ' ' + data[2] + ' ' + data[3];
	return data;
}

myApp.onPageInit('anketa', function (page) {
	
	if(!register_without_card){
		$('.page[data-page=anketa] input[name=password]').parent().remove();
	}
	console.log(register_without_card)
	// page.fromPage.name
	if(register_without_card){
		$('.page[data-page=anketa] .button.big').text('Зарегистрироваться');
		//$('.page[data-page=anketa] .check_promo').parent().remove();
	}
/*
	if(localStorage.getItem('country') && localStorage.getItem('country') != 'KZ'){
		$('.page[data-page=anketa] input[name=promo]').parent().css('display', 'none');
	}
*/
	$('#rules').on('click',function(){
		if(localStorage.getItem('country') == 'KZ'){
			var ref = cordova.InAppBrowser.open( 'docs/kz/index.html', '_blank', 'location=no');
		}else{
			var ref = cordova.InAppBrowser.open( 'docs/rules.html', '_blank', 'location=no');
		}
	});
	
	var d = new Date();
	d.setDate(d.getDate()-(365*30));
	
	var dobCalendar = myApp.calendar({
		input: '#calendar-input',
		//value: [d],
		maxDate: new Date(),
		onlyOnPopover: true,
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Dec'],
		dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		dateFormat: 'dd.mm.yyyy',
		closeOnSelect: true,
		onChange: function (p, values, displayValues){
			dateOk = true;
			$('#calendar-input').parent().find('.hint').css('display','none');
			$('#calendar-input').removeClass('error');
			$('#calendar-input').parent().find('.title').addClass('active');
			$('#calendar-input').addClass('active');
			
			selectedBirthdate = formatDobDate( values[0] + '' );
		},
		onOpen: function(){
			$('#calendar-input').parent().find('.title').addClass('active');
			$('#calendar-input').addClass('active');
		},
		onClose: function(){
			/*
			if(!dateOk){
				$('#calendar-input').parent().find('.title').removeClass('active');
				$('#calendar-input').removeClass('active');
			}
			*/
		}
	}); 
	
	if(myApp.device.ios){
		/*
		$('input').each(function(index, element) {
			$(element).on('focus',function(){
				if($(element).offset().top > ($(document).height()/2) ){
					console.log('scroll');
					$(".page[data-page=anketa] .page-content").animate({
						scrollTop: $(element).offset().top
					}, 500);
				}
			});
		});
		*/
	}
	/*
	$('input').each(function(index, element) {
		if( $(element).val().length > 0 ){
			$(element).parent().find('.title').addClass('active');
		}
	});
	*/
	//$(".page[data-page=anketa] input[type=tel]").mask("+7(999)999-9999");
	
	Inputmask().mask("input[type=tel]");
	
	$('.page[data-page=anketa]').on('mousedown touchstart', function(){
		$('.page[data-page=anketa] input').not('.filled').each(function(index, element) {
			if( $(element).val().length == 0 ){
				$(element).parent().find('.title').removeClass('active');
				$(element).removeClass('active');
			}else{
				$(element).parent().find('.hint').css('display','none');
				$(element).removeClass('error');
				$(element).parent().find('.title').addClass('active');
			}
		});
	});
	
	$('.page[data-page=anketa] input').each(function(index, element) {
        $(element).on('focus keyup',function(){
			$(element).parent().find('.title').addClass('active');
			$(element).addClass('active');
			
			if( $(element).attr('id') != 'calendar-input' ){
				dobCalendar.close();
			}
		});
		/*
		$(element).on('focusout',function(){
			if( $(element).val().length == 0 )
			{
				if( !$(element).hasClass('filled') ){
					$(element).parent().find('.title').removeClass('active');
				}
				$(element).removeClass('active');
			}
			else{
				$(element).parent().find('.hint').css('display','none');
				$(element).removeClass('error');
			}
		});
		*/
    });
	/*
	$('#paymentRules').on('change',function(){
		if( $(this).is(':checked') ){
			$(this).removeClass('error');
		}else{
			$(this).addClass('error');
		}
	});
	*/
	$('.page[data-page=anketa] .button.big').on('click',function(){
		var complete = true;
		$('.page[data-page=anketa] .hint').css('display','none');
		$('.page[data-page=anketa] input').removeClass('error');
		
		if(localStorage.getItem('country') != 'KZ'){
			$('.page[data-page=anketa] input')
			.not('input[name=promo]')
			.not('input[name=birthday]')
			.each(function(index, element) {				
				if($(element).val().length == 0 || ('inputmask' in element && !element.inputmask.isValid())) {
						$(element).addClass('error');
						$(element).parent().find('.hint').css('display','block');
						$(element).parent().find('.title').addClass('active');
						$(element).focus();
						
						complete = false;
						
						return false;
					}
				});
			} else {
				$('.page[data-page=anketa] input')
					.not('input[name=second_name]')
					.not('input[name=promo]')
					.not('input[name=birthday]')
					.each(function(index, element) {								
						if($(element).val().length == 0 || ('inputmask' in element && !element.inputmask.isValid())) {
							$(element).addClass('error');
							$(element).parent().find('.hint').css('display','block');
							$(element).parent().find('.title').addClass('active');
							$(element).focus();
							
							complete = false;
							
							return false;
						}
					});
				}
			
		
		if(complete){
			var verimail = new Comfirm.AlphaMail.Verimail();
			
			verimail.verify( $('.page[data-page=anketa] input[name=email]').val(), function(status, message, suggestion){
				if(status < 0){
					// Incorrect syntax!
					complete = false;
					myApp.alert('Email указан неверно', 'Ошибка', function () {
						$('.page[data-page=anketa] input[name=email]').addClass('error');
						$('.page[data-page=anketa] input[name=email]').parent().find('.title').addClass('active');
						$('.page[data-page=anketa] input[name=email]').focus();
					});
				}else{
					// Syntax looks great!
				}
			});
		}
		
		if( complete && $('#paymentRulesA').css('display') == 'block' ){
			myApp.alert('Необходимо ознакомиться с Правилами абонентского юридического обслуживания', 'Внимание', function () {});
			//$('#paymentRules').addClass('error');
			complete = false;
		}
		/*
		if(complete && !dateOk){
			$('#calendar-input').addClass('error');
			$('#calendar-input').parent().find('.hint').css('display','block');
			dobCalendar.open();
			complete = false;
		}
		*/
		if(complete){
			if(register_without_card){
				$.ajax({
					method: "POST",
					url: cordovaMode?"https://lk.urist.els24.com/api/registration-simple":"echo_registration_simple.php",
					timeout: 10000,
					data: { 
						last_name : $('.page[data-page=anketa] input[name=last_name]').val(),
						first_name : $('.page[data-page=anketa] input[name=first_name]').val(),
						second_name : $('.page[data-page=anketa] input[name=second_name]').val(),
						//birthday : $('.page[data-page=anketa] input[name=birthday]').val(),
						phone : $('.page[data-page=anketa] input[name=phone]').val(),
						email : $('.page[data-page=anketa] input[name=email]').val(),
						city : $('.page[data-page=anketa] input[name=city]').val(),
						// promo_code: $('.page[data-page=anketa] input[name=promo]').val(),
						password : $('.page[data-page=anketa] input[name=password]').val(),
						country: localStorage.getItem('country')
					},
					beforeSend: function() {
						showLoading();
					}
				})
				.done(function( msg ) {
					console.log( msg );
					if( msg.status == "success" )
					{
						myApp.alert('Благодарим Вас за выбор сервиса Личный Юрист!', 'Регистрация прошла успешно', function () {
							mainView.router.loadPage({
								url: 'login.html',
								animatePages: true
							});
						});
					}
					else if( msg.status == "failure" ){
						myApp.alert(msg.message, 'Ошибка регистрации', function () {});
					}
				})
				.fail(function() {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				})
				.always(function() {
					hideLoading();
				});
			}else{
				$.ajax({
					method: "POST",
					url: cordovaMode?"https://lk.urist.els24.com/api/registration":"echo_registration.php",
					timeout: 10000,
					data: { 
						last_name : $('.page[data-page=anketa] input[name=last_name]').val(),
						first_name : $('.page[data-page=anketa] input[name=first_name]').val(),
						second_name : $('.page[data-page=anketa] input[name=second_name]').val(),
						phone : $('.page[data-page=anketa] input[name=phone]').val(),
						email : $('.page[data-page=anketa] input[name=email]').val(),
						city : $('.page[data-page=anketa] input[name=city]').val(),
						//birthday : $('.page[data-page=anketa] input[name=birthday]').val(),
						package_id : selected_product,
						family_option: ( familyOption?1:0 ),
						// promo_code: $('.page[data-page=anketa] input[name=discount]').val(),
					},
					beforeSend: function() {
						showLoading();
					}
				})
				.done(function( msg ) {
					console.log( msg );
					if( msg.status == "success" && msg.paymentUrl != null )
					{
						myApp.alert('Сейчас Вы будете направлены на страницу оплаты', 'Регистрация прошла успешно', function () {
							var ref = cordova.InAppBrowser.open( msg.paymentUrl, '_blank', 'closebuttoncaption=Готово');

							ref.addEventListener('exit', function(){
								mainView.router.loadPage({
								url: 'login.html',
									animatePages: true
								});
							});
						});
					}
					else if( msg.status == "failure" ){
						myApp.alert(msg.message, 'Ошибка регистрации', function () {});
					}
				})
				.fail(function() {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				})
				.always(function() {
					hideLoading();
				});
			}	
		}
	});
	
	var agree = true;
	$('#paymentRulesA').on('click', function(){
		$('#paymentRulesA').css('display','none');
		$('#paymentRulesB').css('display','block');
		agree = false;
	});
	$('#paymentRulesB').on('click', function(){
		$('#paymentRulesA').css('display','block');
		$('#paymentRulesB').css('display','none');
		agree = true;
	});

	$('.page[data-page=anketa] .check_promo').on('click',function(){
		
		if( $('.page[data-page=anketa] .check_promo').parent().find('input').val().length == 0){
			myApp.alert('Пожалуйста, введите промо-код', 'Ошибка', function () {
				$('.page[data-page=anketa] .check_promo').parent().find('input').focus();
			});
		}else{
			$.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/check-promocode":"echo_check_promocode.php",
				timeout: 10000,
				data: { 
					id : selected_product,
					// promo_code : $('.page[data-page=anketa] input[name=discount]').val()
				},
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" )
				{
					myApp.alert("Код активирован. Цена с учетом скидки: " + msg.new_price, 'Спасибо');
					$('.page[data-page=anketa] .check_promo').remove();
				}
				else if( msg.status == "failure" ){
					myApp.alert(msg.message, "Ошибка активации");
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		}	
	});
});