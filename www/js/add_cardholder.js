myApp.onPageInit('add_cardholder', function (page) {
	var verimail = new Comfirm.AlphaMail.Verimail();
	
	Inputmask().mask("input[type=tel]");
	
	var d = new Date();
	d.setDate(d.getDate()-(365*30));
	
	var dobCalendar = myApp.calendar({
		input: '#calendar-input',
		//value: [d],
		maxDate: new Date(),
		onlyOnPopover: true,
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Dec'],
		dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
		dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		dateFormat: 'dd.mm.yyyy',
		closeOnSelect: true,
		onChange: function (p, values, displayValues){
			dateOk = true;
			$('#calendar-input').parent().find('.hint').css('display','none');
			$('#calendar-input').removeClass('error');
			$('#calendar-input').parent().find('.title').addClass('active');
			$('#calendar-input').addClass('active');
			
			selectedBirthdate = formatDobDate( values[0] + '' );
		},
		onOpen: function(){
			$('#calendar-input').parent().find('.title').addClass('active');
			$('#calendar-input').addClass('active');
		},
		onClose: function(){
			/*
			if(!dateOk){
				$('#calendar-input').parent().find('.title').removeClass('active');
				$('#calendar-input').removeClass('active');
			}
			*/
		}
	}); 
	
	$('.page[data-page=add_cardholder]').on('mousedown touchstart', function(){
		$('.page[data-page=add_cardholder] input').not('.filled').each(function(index, element) {
			if( $(element).val().length == 0 ){
				$(element).parent().find('.title').removeClass('active');
				$(element).removeClass('active');
			}else{
				$(element).parent().find('.hint').css('display','none');
				$(element).removeClass('error');
			}
		});
	});
	
	$('.page[data-page=add_cardholder] input').each(function(index, element) {
        $(element).on('focus',function(){
			$(element).parent().find('.title').addClass('active');
			$(element).addClass('active');
			
			if( $(element).attr('id') != 'calendar-input' ){
				dobCalendar.close();
			}
		});
    });
	
	if( localStorage.getItem("users") )
	{
		var users = JSON.parse( localStorage.getItem("users") );
		
		if(selected_card_for_user_to_add != null){
			$('.page[data-page=add_cardholder] .sbcard img:not(.arrow)').attr('src', selected_card_for_user_to_add.image );
			$('.page[data-page=add_cardholder] .sbcard .cardnum').text( selected_card_for_user_to_add.code );
			$('.page[data-page=add_cardholder] .sbcard .tarif').text( selected_card_for_user_to_add.package_title );
			$('.page[data-page=add_cardholder] .sbcard').attr('data-id', selected_card_for_user_to_add.id );
		}else{
			var firstActiveCard = false;

			$(users.cards).each(function(index, element) {
				if( true ){
					firstActiveCard = element;
					return false;
				}
			});
			
			$('.page[data-page=add_cardholder] .sbcard img:not(.arrow)').attr('src', firstActiveCard.image );
			$('.page[data-page=add_cardholder] .sbcard .cardnum').text( firstActiveCard.code );
			$('.page[data-page=add_cardholder] .sbcard .tarif').text( firstActiveCard.package_title );
			$('.page[data-page=add_cardholder] .sbcard').attr('data-id', firstActiveCard.card_id );
		}
			
		
		$('.sbcardlist .sbcard:not(.template)').each(function(index, element) {
			$(element).remove();
		});
		
		$(users.cards).each(function(index, element) {
			if( true ){
				var template = $('#sbcardTemplate').clone();

				$(template).find('img').attr('src', element.image );
				$(template).find('.cardnum').text( element.code );
				$(template).find('.tarif').text( element.package_title );

				$(template).on('click',function(){
					
					$('.page[data-page=add_cardholder] .sbcard img:not(.arrow)').attr('src', element.image );
					$('.page[data-page=add_cardholder] .sbcard .cardnum').text( element.code );
					$('.page[data-page=add_cardholder] .sbcard .tarif').text( element.package_title );
					$('.page[data-page=add_cardholder] .sbcard').attr('data-id', element.id );

					$('.selectmenu.sbcards').removeClass('active');

					$('.bg').fadeOut();
				});

				$(template).removeClass('template');
				$(template).removeAttr('id');

				$('.sbcardlist').append( template );
			}
		});
	}
	
	$('.selectmenu.sbcards').css('bottom', -( $('.selectmenu.sbcards').height() + 40 ) );
	
	$('.page[data-page=add_cardholder] .sbcard').on('click',function(){
		$('.selectmenu.sbcards').addClass('active');
		$('.bg').fadeIn();
	});
	
	$('.page[data-page=add_cardholder] .button.big').on('click',function(){
		var complete = true;
		$('.page[data-page=add_cardholder] .hint').css('display','none');
		$('.page[data-page=add_cardholder] input').removeClass('error');
		
		$('.page[data-page=add_cardholder] input').each(function(index, element) {
            if( $(element).val().length == 0 )
			{
				$(element).addClass('error');
				$(element).parent().find('.hint').css('display','block');
				$(element).parent().find('.title').addClass('active');
				$(element).focus();
				
				complete = false;
				
				return false;
			}
        });
		
		if(complete){
			verimail.verify( $('.page[data-page=add_cardholder] input[name=email]').val(), function(status, message, suggestion){
				if(status < 0){
					// Incorrect syntax!
					complete = false;
					myApp.alert('Email указан неверно', 'Ошибка', function () {
						$('.page[data-page=add_cardholder] input[name=email]').addClass('error');
						$('.page[data-page=add_cardholder] input[name=email]').parent().find('.title').addClass('active');
						$('.page[data-page=add_cardholder] input[name=email]').focus();
					});
				}else{
					// Syntax looks great!
				}
			});
		}
		
		if(complete){
			$.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/family-users":"echo_family_users.php",
				data: { 
					'token': localStorage.getItem('token'),
					'card_id': $('.page[data-page=add_cardholder] .sbcard').attr('data-id'),
					'last_name': $('.page[data-page=add_cardholder] input[name=surname]').val(),
					'first_name': $('.page[data-page=add_cardholder] input[name=name]').val(),
					'second_name': $('.page[data-page=add_cardholder] input[name=secname]').val(),
					'phone': $('.page[data-page=add_cardholder] input[name=phone]').val(),
					'email': $('.page[data-page=add_cardholder] input[name=email]').val(),
					'birthday': $('.page[data-page=add_cardholder] input[name=dob]').val(),
					'sex': '',
					'city': $('.page[data-page=add_cardholder] input[name=city]').val()
				},
				timeout: 10000,
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log(msg);
				if( msg.status == 'success' )
				{
					myApp.alert('Пользователю направлено смс и email с доступом к сервису', 'Спасибо', function () {
						mainView.router.loadPage({
							url: 'cards.html',
							animatePages: true
						});
					});
				}
				else if( msg.status == 'failure' ){
					myApp.alert(msg.message, 'Ошибка', function () {});
				}
				else
				{
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				}
			})
			.fail(function(msg) {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		}
	});
});