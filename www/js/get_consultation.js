var create_consultation_clicked = false;
var created_consultation;

function updateServices(card){
	var services_html = '';
	
	$(card.services).each(function(index, element) {
		if( element.available > 0 )
		{
			services_html += '<a href="#"><div class="item" data-value="' + element.id + '">' + element.title + '</div></a>';
		}
	});
	
	$('.listview.services .wrapper').html(services_html);
	$('.listview.services .item').off('click');
	$('.listview.services .item').on('click',function(){
		$('.page[data-page=get_consultation] .service span').text( $(this).text() );
		$('.page[data-page=get_consultation] .service').attr('data-selected-service', $(this).attr('data-value') );
		$('.bg, .listview.services').fadeOut();

		if( $(this).attr('data-value') == 14 ){

			$.ajax({
				method: "GET",
				url: cordovaMode?"https://lk.urist.els24.com/api/rcm":"echo_rcm.php",
				data: { 
					'token': localStorage.getItem('token')
				},
				timeout: 10000,
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" )
				{
					$('.page[data-page=get_consultation] .documents').parent().css('display','block');
					$('.page[data-page=get_consultation] .documents').off('click');
					$('.page[data-page=get_consultation] .documents').on('click',function(){
						$('.bg, .listview.documentlist').css('display','block');
					});

					var documents_html = '';

					for(var i in msg.data.category){
						var key = i;
						var category = msg.data.category[i];

						documents_html += '<div class="item section">' + category + '</div>';

						for(var j in msg.data.items[key]){
							var sub_key = j;
							var doc = msg.data.items[key][j];
							documents_html += '<a href=""><div class="item" data-id="' + doc.id + '">' + doc.title + '</div></a>';
						}
					}

					$('.listview.documentlist .wrapper').html('');
					$('.listview.documentlist .wrapper').html(documents_html);

					$('.listview.documentlist .item').off('click');
					$('.listview.documentlist .item').on('click',function(){
						$('.page[data-page=get_consultation] .documents span').text( $(this).text() );
						$('.page[data-page=get_consultation] .documents').attr('data-selected-document', $(this).attr('data-id') );
						$('.listview.documentlist, .bg').fadeOut();
					});
				}
				else
				{
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		}else{
			$('.page[data-page=get_consultation] .documents').parent().css('display','none');
		}
	});
}

myApp.onPageInit('get_consultation', function (page) {

	$('#newyear').fadeOut(1);

	if( localStorage.getItem('country') && localStorage.getItem('country') == 'KZ' ){
		$('.page[data-page=get_consultation] .country').text('Казахстан');
	}
	
	if(cordovaMode && myApp.device.android){
		var permissions = cordova.plugins.permissions;

		permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function( status ){
			if ( status.hasPermission ) {
			}
			else {
				permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE);
			}
		});
	}
	
	$('.page[data-page=get_consultation] .service').on('click',function(){
		$('.bg, .listview.services').css('display','block');
	});
	
	$('.page[data-page=get_consultation] textarea').on('focus',function(){
		mainView.hideToolbar(false);
	});

	$('.page[data-page=get_consultation] textarea').on('focusout',function(){
		mainView.showToolbar(false);
	});
	
	if( localStorage.getItem("cards") )
	{
		cards = JSON.parse( localStorage.getItem("cards") );
		
		var firstActiveCard = false;
		$(cards).each(function(index, element) {
			if( element.status_id == 2 ){
				firstActiveCard = element;
				return false;
			}
		});
		
		$('.page[data-page=get_consultation] .sbcard img:not(.arrow)').attr('src', firstActiveCard.image );
		$('.page[data-page=get_consultation] .sbcard .cardnum').text( firstActiveCard.code );
		$('.page[data-page=get_consultation] .sbcard .tarif').text( firstActiveCard.package_title );
		$('.page[data-page=get_consultation] .sbcard').attr('data-id', firstActiveCard.id );
		
		updateServices(firstActiveCard);
		
		$('.sbcardlist .sbcard:not(.template)').each(function(index, element) {
			$(element).remove();
		});
		
		$(cards).each(function(index, element) {
			if( element.status_id == 2 ){
				var template = $('#sbcardTemplate').clone();

				$(template).find('img').attr('src', element.image );
				$(template).find('.cardnum').text( element.code );
				$(template).find('.tarif').text( element.package_title );

				$(template).on('click',function(){
					$('.page[data-page=get_consultation] .service span').text( 'Выберите услугу' );
					$('.page[data-page=get_consultation] .service').removeAttr('data-selected-service');
					
					$('.page[data-page=get_consultation] .documents span').text( 'Выберите документ' );
					$('.page[data-page=get_consultation] .documents').removeAttr('data-selected-document');
					$('.page[data-page=get_consultation] .documents').parent().css('display','none');
					
					$('.page[data-page=get_consultation] .sbcard img:not(.arrow)').attr('src', element.image );
					$('.page[data-page=get_consultation] .sbcard .cardnum').text( element.code );
					$('.page[data-page=get_consultation] .sbcard .tarif').text( element.package_title );
					$('.page[data-page=get_consultation] .sbcard').attr('data-id', element.id );
					
					updateServices(element);

					$('.selectmenu.sbcards').removeClass('active');

					$('.bg').fadeOut();
				});

				$(template).removeClass('template');
				$(template).removeAttr('id');

				$('.sbcardlist').append( template );
			}
		});
	}
	
	$('.selectmenu.sbcards').css('bottom', -( $('.selectmenu.sbcards').height() + 40 ) );
	
	$('.page[data-page=get_consultation] .sbcard').on('click',function(){
		$('.selectmenu.sbcards').addClass('active');
		$('.bg').fadeIn();
	});
	
	$('.page[data-page=get_consultation] .button.big').on('click',function(){
		if( !create_consultation_clicked )
		{
			$('.page[data-page=get_consultation] textarea, .page[data-page=get_consultation] .service').removeClass('error');
			$('.page[data-page=get_consultation] .hint').css('display','none');
			
			if( parseInt( $('.page[data-page=get_consultation] .service').attr('data-selected-service') ) == 0 )
			{
				$('.page[data-page=get_consultation] .service').addClass('error');
				$('.page[data-page=get_consultation] .service').parent().find('.hint').css('display','block');
			}
			else if( $('.page[data-page=get_consultation] textarea').val().length == 0 )
			{
				$('.page[data-page=get_consultation] textarea').focus();
				$('.page[data-page=get_consultation] textarea').addClass('error');
				$('.page[data-page=get_consultation] textarea').parent().find('.hint').css('display','block');
			}
			else
			{
				var formData = new FormData();
				formData.append('token', localStorage.getItem('token') );
				formData.append('card_id', $('.page[data-page=get_consultation] .sbcard').attr('data-id') );
				formData.append('description', $('.page[data-page=get_consultation] textarea').val() );
				formData.append('services_id', $('.page[data-page=get_consultation] .service').attr('data-selected-service') );
				formData.append('contract_id', $('.page[data-page=get_consultation] .documents').attr('data-selected-document') );
				
				if( localStorage.getItem('country') && localStorage.getItem('country') == 'KZ' ){
					var low_country_id = 3;
				}else{
					var low_country_id = 1;
				}

				formData.append('low_country_id', low_country_id );
				
				listAddedFiles.forEach(function(file) {
					formData.append('files[]', file);
				})
				

				$.ajax({
					type: "POST",
					url: cordovaMode?"https://lk.urist.els24.com/api/consultation":"echo_create_consultation.php",
					data: formData,
					timeout: 30000,
					contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
					processData: false, // NEEDED, DON'T OMIT THIS
					beforeSend: function() {
						showLoading();
					}
				})
				.done(function( msg ) {
					console.log( msg );
					if( msg.status == "success" )
					{
						created_consultation = msg.task_id;
						
						if(cordovaMode){
							window.ga.trackEvent('GetConsultation', $('.page[data-page=get_consultation] .service span').text() );
						}
						
						$.ajax({
							method: "GET",
							url: cordovaMode?"https://lk.urist.els24.com/api/consultation":"consul_echo.php",
							data: { 
								'token': localStorage.getItem('token')
							},
							timeout: 10000,
							beforeSend: function() {
								showLoading();
							}
						})
						.done(function( msg ) {
							console.log( msg );
							if( msg.status == "success" )
							{
								localStorage.setItem("consultations", JSON.stringify(msg.data));
								
								if( localStorage.getItem("mb") == 0){
									var targetUrl = 'https://lk.urist.els24.com/api/single-consultation';
								}else{
									var targetUrl = 'https://lk.urist.els24.com/api/mb-single-consultation';
								}
								
								$.ajax({
									method: "GET",
									url: cordovaMode?targetUrl:"consul_one_echo.php",
									data: { 
										'token': localStorage.getItem('token'),
										'task_id': created_consultation
									},
									timeout: 10000,
									beforeSend: function() {
										showLoading();
									}
								})
								.done(function( msg ) {
									console.log( msg );
									if( msg.status == "success" )
									{
										consultations = JSON.parse( localStorage.getItem("consultations") );
										$(consultations).each(function(index, element) {
											if( element.id == msg.data.task.id )
											{
												element.comments = msg.data.comments;
												element.description = msg.data.task.description;
												element.rating = msg.data.task.rating;
												element.low_country_title = msg.data.task.low_country_title;
												element.card_code = msg.data.task.card_code;
												element.access_setting = msg.data.access_setting;
												
												current_consultation = element;
											}
										});
										
										localStorage.setItem("consultations", JSON.stringify(consultations));
										selected_consultation = created_consultation;
										
										var messageText = '';
										$(serviceMessages).each(function(index,element){
											if( element.service == $('.page[data-page=get_consultation] .service span').text() ){
												if( $('.page[data-page=get_consultation] .sbcard .tarif').text() == 'Базовый' || $('.page[data-page=get_consultation] .sbcard .tarif').text() == 'Премьер'){
													messageText = element.message;
												}else{
													messageText = element.premiumMessage;
												}
											}
										})
										
										if(messageText != ''){ 
											myApp.alert(messageText, 'Спасибо', function () {
												mainView.router.loadPage('chat.html');
											});
										}else{
											mainView.router.loadPage('chat.html');
										}
									}
								})
								.fail(function() {
									myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
								})
								.always(function() {
									hideLoading();
								});
							}
							else
							{
								myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
							}
						})
						.fail(function() {
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						})
						.always(function() {
							hideLoading();
						});
					}
					else
					{
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					}
				})
				.fail(function() {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				})
				.always(function() {
					//$('.page[data-page=get_consultation] .button.big').css('opacity',1.0);
					//create_consultation_clicked = false;
					hideLoading();
				});
			}
		}
	});

	// FILE GET CONSULTATION

	var $containerFiles = $('.fix-file-input')
	var $btnAddFile = $containerFiles.find('.fix-file-input__btn')		
	var $htmlListFiles = $containerFiles.find('.fix-file-input__list')		
	var $srcFiles = $containerFiles.find('.fix-file-input__src').get(0)
	var listAddedFiles = []

	const renderFileList = function () {
		$htmlListFiles.empty()
		listAddedFiles.forEach(function(file, index) {			
			$htmlListFiles.append(
				'<li class="fix-file-input__item" data-index="'+ index + '">' +
					'<span class="fix-file-input__title">' + file.name + '</span>' +
					'<i class="fix-file-input__remove"></i>' +
				'</li>'
			)
		})
	}
	$('body').on('click', '.fix-file-input__remove', function(e) {
		var index = $(this).parent().data('index')		
		listAddedFiles.splice(index, 1)		
		$htmlListFiles.find('li[data-index="'+ index + '"]').remove()
		renderFileList()

		if (listAddedFiles.length === 0) {
			$btnAddFile.removeClass('withfile');	
		}
	})

	$('#imageHolder').on('change',function(){		
		var files = Array.prototype.slice.call($srcFiles.files)
		listAddedFiles = listAddedFiles.concat(files)
		$srcFiles.value = ''
		var hasFiles = listAddedFiles.length > 0
			
		if (hasFiles){
			renderFileList()
			$btnAddFile.addClass('withfile');	
		}
	});

	if (selected_card_to_get_consultation != null) {

		var selectedCard = cards[selected_card_to_get_consultation];
		
		$('.page[data-page=get_consultation] .sbcard img:not(.arrow)').attr('src', selectedCard.image );
		$('.page[data-page=get_consultation] .sbcard .cardnum').text( selectedCard.code );
		$('.page[data-page=get_consultation] .sbcard .tarif').text( selectedCard.package_title );
		$('.page[data-page=get_consultation] .sbcard').attr('data-id', selectedCard.id );
		
		updateServices(selectedCard);

		if(selected_consultation_to_get_consultation != null){
			$('.page[data-page=get_consultation] .service span').text( selected_consultation_to_get_consultation.name );
			$('.page[data-page=get_consultation] .service').attr('data-selected-service', selected_consultation_to_get_consultation.id );
		}
	}
});