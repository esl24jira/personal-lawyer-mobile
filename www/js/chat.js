var myMessages;
var myMessagebar;
var message_sent = false;
var messageText = '';
var fileName = '';

function parseChat() {
	myMessages.clean();

	$(current_consultation.comments).each(function (index, element) {

		if (element.files.length > 0) {

			var messageText = (element.comment == null) ? '' : element.comment;

			$(element.files).each(function (index2, element2) {
				var file_extension = element2.name.split('.');
				file_extension = file_extension[1];
				file_extension = file_extension.toLowerCase();

				if (element.is_company == 1) {
					if (file_extension == 'png' || file_extension == 'jpeg' || file_extension == 'jpg' || file_extension == 'gif' || file_extension == 'bmp') {
						messageText += '<div><img src="https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + element2.id + ')</div>';
					} else if (file_extension == 'mp3') {
						messageText += '<div><img class="audio_player" src="img/play-button.svg" onclick="playaudio(this);" ><img onclick="rewind_audio(this);" class="audio_player" src="img/rewind.svg"><audio oncanplay="canplayaudio(this);" data-play="false" preload="auto"><source src="https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + element2.id + '" type="audio/mp3"></audio></div>';
					} else {
						messageText += '<div class="fileclick"><img class="clip" data-id="' + element2.id + '" data-extension="' + file_extension + '" src="img/Icons/chat-attach-white.svg"><p>' + element2.original_name + '</p></div>';
					}
				} else {
					if (file_extension == 'png' || file_extension == 'jpeg' || file_extension == 'jpg' || file_extension == 'gif' || file_extension == 'bmp') {
						messageText += '<img data-extension="' + file_extension + '" src="https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + element2.id + '">';
					} else {
						messageText += '<div><img class="clip" data-id="' + element2.id + '" data-extension="' + file_extension + '" src="img/Icons/chat-attach-white.svg"></div>';
					}
				}
			});
		} else {
			var messageText = element.comment;
		}

		if (element.is_company == 1) {
			myMessages.prependMessage({
				text: messageText,
				type: element.is_company == 1 ? 'received' : 'sent',
				name: element.is_company == 1 ? 'Личный Юрист' : null,
				date: element.created_at,
				// time: (new Date(element.created_at)).getHours() + ':' + (new Date(element.created_at)).getMinutes()			
			});
		} else {
			myMessages.prependMessage({
				text: messageText,
				type: element.is_company == 1 ? 'received' : 'sent',
				name: element.is_company == 1 ? 'Личный Юрист' : null,
				date: element.created_at,
				label: element.status_id == 2 ? 'Прочитано' : 'Доставлено',
				// time: (new Date(element.created_at)).getHours() + ':' + (new Date(element.created_at)).getMinutes()			
			});
		}
	});
	/*
		myMessages.prependMessage({
			text: current_consultation.description,
			type: 'sent',
			date: current_consultation.created_at_ts
		});
	*/
	$('.message .message-text .fileclick').not('.audio_player').off('click');
	$('.message .message-text .fileclick').not('.audio_player').on('click', function () {
		if (
			$(this).find('img').attr('data-extension') == 'png' ||
			$(this).find('img').attr('data-extension') == 'jpeg' ||
			$(this).find('img').attr('data-extension') == 'jpg' ||
			$(this).find('img').attr('data-extension') == 'bmp' ||
			$(this).find('img').attr('data-extension') == 'gif'
		) {
			if (myApp.device.android) {
				PhotoViewer.show($(this).find('img').attr('src'));
			}
		}
		//else if( $(this).attr('data-extension') == 'doc' ){
		else {
			var file_id = $(this).find('img').attr('data-id');

			myApp.confirm('Отправить документ на email?', 'Отправка файла',
				function () {
					$.ajax({
							method: "POST",
							url: cordovaMode ? "https://lk.urist.els24.com/api/send-file" : "echo_send_file.php",
							data: {
								'token': localStorage.getItem('token'),
								'file_id': file_id
							},
							timeout: 10000,
							beforeSend: function () {
								showLoading();
							}
						})
						.done(function (msg) {
							console.log(msg);
							if (msg.status == 'success') {
								myApp.alert('Файл отправлен на почту', 'Спасибо', function () {});
							} else {
								myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
							}
						})
						.fail(function () {
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						})
						.always(function () {
							hideLoading();
						});
				}
			);
		}
		/*
		else
		{
			window.open('https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + $(this).attr('data-id'), '_system');
		}
		*/
	});
}

function currentTime() {
	var dt = new Date();
	var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
	return time;
}

myApp.onPageBack('chat', function(e) {				
	$('#mainToolbar').removeClass('toolbar-hidden');
	$('.navbar .back').removeClass('js-chat-link-back');
})

myApp.onPageInit('chat', function (page) {
	var conversationStarted = false;

	message_sent = false;

	if (cordovaMode && myApp.device.android) {
		$('.page-content').on('click', function () {
			NativeKeyboard.hideMessengerKeyboard();
		});

		var permissions = cordova.plugins.permissions;

		permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function (status) {
			if (status.hasPermission) {} else {
				permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE);
			}
		});	
	}

	$('.navbar .back').addClass('js-chat-link-back');	

	$('.page[data-page=chat] .file').on('click', function () {
		$('#chatAttachment').click();
	});

	if (myApp.device.android) {
		$('#commentMessage').on('input propertychange keyup', function (event) {
			$('.round').css('margin-bottom', ($('.toolbar.messagebar').height() + 25) + 'px');
		});
	}

	$('#chatAttachment').on('change', function () {
		var file_name = $('#chatAttachment').val();

		if (file_name.indexOf('/') >= 0) {
			file_name = file_name.split('/');
		} else {
			file_name = file_name.split('\\');
		}

		file_name = file_name[file_name.length - 1];

		if (myApp.device.android) {
			myMessagebar.value(myMessagebar.value() + ' Прикреплен файл: ' + file_name);
		} else {
			NativeKeyboard.updateMessenger({
				text: messageText + ' Прикреплен файл: ' + file_name
			});
		}
	});

	myMessagebar = myApp.messagebar('.js-chat-messagebar', {
		maxHeight: 200
	});			
	
	myMessages = myApp.messages('[data-page=chat] .js-chat-messages', {
		autoLayout: true,
		scrollMessages: true
	});

	parseChat();

	$('#mainToolbar').addClass('toolbar-hidden');

	$('.consNum').text(current_consultation.id);


	$('#sendSound').on('click', function () {

		if ($('#commentMessage').val().length == 0) {
			$('#commentMessage').focus();
		} else if (!message_sent) {
			var formData3 = new FormData();
			formData3.append('token', localStorage.getItem('token'));
			formData3.append('task_id', selected_consultation);
			formData3.append('comment', $('#commentMessage').val());

			formData3.append('image[]', document.getElementById("chatAttachment").files[0]);

			$.ajax({
					type: "POST",
					url: cordovaMode ? "https://lk.urist.els24.com/api/comment" : "echo_send_message.php",
					data: formData3,
					timeout: 15000,
					contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
					processData: false, // NEEDED, DON'T OMIT THIS
					beforeSend: function () {
						$('#commentMessage').css('color', '#e0e0e0');
						message_sent = true;
						showLoading();
					},
					complete: function (xhr, textStatus) {
						//alert("complete: " + xhr.status);
						hideLoading();
					}
				})
				.done(function (msg) {
					console.log(msg);
					//alert( "done: " + JSON.stringify(msg) );
					if (msg.status == 'success') {
						/*
						myMessages.addMessage({
							text: $('#commentMessage').val(),
							type: 'sent',
							name: null,
							date: currentTime()
						});
					
						*/
						
						myMessagebar.clear();
						$('#chatAttachment').val('');
						hideLoading();
						if (myApp.device.android) {
							$('.round').css('margin-bottom', (46 + 25) + 'px');
						}
					} else {
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						hideLoading();
					}
				})
				.fail(function (dt, status, request) {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					hideLoading();
				})
				.always(function (dt, status, request) {
					//alert( "always: " + request.getAllResponseHeaders() );
					$('#commentMessage').css('color', '#000000');
					message_sent = false;
					hideLoading();
				});
		}
	});

	$('.round').on('click', function (event) {
		if ($('.roundMenu').css('display') == 'block') {
			$('.roundMenu').fadeOut('300', function () {});
			$('.round').css('background-image', 'url(img/chat-menu.svg)');
			$('.chatbg').fadeOut(300, function () {});
		} else {
			$('.roundMenu').fadeIn('300', function () {});
			$('.round').css('background-image', 'url(img/Icons/nav-close-green.svg)');
			$('.chatbg').fadeIn(300, function () {});
		}
	});

	$('#call_yurist').on('click', function (event) {
		$('#popupCall').click();

		$('.roundMenu').fadeOut('300', function () {});
		$('.round').css('background-image', 'url(img/chat-menu.svg)');
		$('.chatbg').fadeOut(300, function () {});
		/*
				if(myApp.device.ios){
					NativeKeyboard.hideMessenger({
					  animated: false 
					});
				}
		*/
	});

	$('#attach_document').on('click', function (event) {
		$('#chatAttachment').click();

		$('.roundMenu').fadeOut('300', function () {});
		$('.round').css('background-image', 'url(img/chat-menu.svg)');
		$('.chatbg').fadeOut(300, function () {});
	});

	//STATUSBAR
	initChatInterface()	

	update_chat();
	updateChatInterval = setInterval(function () {
		update_chat();
	}, 5000);

});

function playaudio(obj) {
	if ($(obj).parent().find('audio').attr('data-play') == 'true') {
		$(obj).parent().find('audio')[0].play();
		$(obj).attr('src', 'img/pause.svg');
		$(obj).parent().find('audio').attr('data-play', 'playing');
	} else if ($(obj).parent().find('audio').attr('data-play') == 'playing') {
		$(obj).parent().find('audio')[0].pause();
		$(obj).attr('src', 'img/play-button.svg');
		$(obj).parent().find('audio').attr('data-play', 'paused');
	} else if ($(obj).parent().find('audio').attr('data-play') == 'paused') {
		$(obj).parent().find('audio')[0].play();
		$(obj).attr('src', 'img/pause.svg');
		$(obj).parent().find('audio').attr('data-play', 'playing');
	}
}

function rewind_audio(obj) {
	$(obj).parent().find('audio')[0].pause();
	$(obj).parent().find('audio')[0].currentTime = 0;
	$(obj).parent().find('img:first-of-type').attr('src', 'img/play-button.svg');
	$(obj).parent().find('audio').attr('data-play', 'true');
}

function canplayaudio(obj) {
	$(obj).parent().find('img').css('opacity', 1);
	$(obj).attr('data-play', 'true');
}

function update_chat() {
	$.ajax({
			method: "GET",
			url: cordovaMode ? "https://lk.urist.els24.com/api/comment" : "echo_comments.php",
			data: {
				'token': localStorage.getItem('token'),
				'task_id': selected_consultation
			},
			timeout: 3000,
			beforeSend: function () {

			}
		})
		.done(function (msg) {
			console.log(msg);
			if (msg.status == 'success') {
				if (!current_consultation.comments) {
					current_consultation.comments = [];
				}

				if (msg.data.length > current_consultation.comments.length) {
					for (i = 0; i < (msg.data.length - current_consultation.comments.length); i++) {
						if (msg.data[i].files.length > 0) {
							var messageText = msg.data[i].comment;

							$(msg.data[i].files).each(function (index2, element2) {
								var file_extension = element2.name.split('.');
								file_extension = file_extension[1];
								file_extension = file_extension.toLowerCase();

								if (msg.data[i].is_company == 1) {
									if (file_extension == 'png' || file_extension == 'jpeg' || file_extension == 'jpg' || file_extension == 'gif' || file_extension == 'bmp') {
										messageText += '<div><img data-id="' + element2.id + '" data-extension="' + file_extension + '" src="https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + element2.id + ')</div>';
									} else {
										messageText += '<div><img class="clip" data-id="' + element2.id + '" data-extension="' + file_extension + '" src="img/Icons/chat-attach-white.svg"><p>' + element2.original_name + '</p></div>';
									}
								} else {
									if (file_extension == 'png' || file_extension == 'jpeg' || file_extension == 'jpg' || file_extension == 'gif' || file_extension == 'bmp') {
										messageText += '<img data-id="' + element2.id + '" data-extension="' + file_extension + '" src="https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + element2.id + '">';
									} else {
										messageText += '<div><img class="clip" data-id="' + element2.id + '" data-extension="' + file_extension + '" src="img/Icons/chat-attach-white.svg"></div>';
									}
								}
							});
						} else {
							var messageText = msg.data[i].comment;
						}

						if (msg.data[i].is_company == 1) {
							myMessages.addMessage({
								text: messageText,
								type: msg.data[i].is_company == 1 ? 'received' : 'sent',
								name: msg.data[i].is_company == 1 ? 'Личный Юрист' : null,
								date: formatDate(msg.data[i].created_at, 'datetime')
							});
						} else {
							myMessages.addMessage({
								text: messageText,
								type: msg.data[i].is_company == 1 ? 'received' : 'sent',
								name: msg.data[i].is_company == 1 ? 'Личный Юрист' : null,
								date: formatDate(msg.data[i].created_at, 'datetime'),
								label: msg.data[i].status_id == 2 ? 'Прочитано' : 'Доставлено'
							});
						}

						resetScrollChat(true);	

						$('.message .message-text img').off('click');
						$('.message .message-text img').on('click', function () {
							if ($(this).attr('data-extension') == 'doc') {
								var file_id = $(this).attr('data-id');

								myApp.confirm('Документ в формате Word. Отправить на email?', 'Отправка на email',
									function () {
										$.ajax({
												method: "POST",
												url: cordovaMode ? "https://lk.urist.els24.com/api/send-file" : "echo_send_file.php",
												data: {
													'token': localStorage.getItem('token'),
													'file_id': file_id
												},
												timeout: 10000,
												beforeSend: function () {
													showLoading();
												}
											})
											.done(function (msg) {
												console.log(msg);
												if (msg.status == 'success') {
													myApp.alert('Файл отправлен на почту', 'Спасибо', function () {});
												} else {
													myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
												}
											})
											.fail(function () {
												myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
											})
											.always(function () {
												hideLoading();
											});
									}
								);
							} else if ($(this).attr('data-extension') == 'png' || $(this).attr('data-extension') == 'jpeg' || $(this).attr('data-extension') == 'jpg' || $(this).attr('data-extension') == 'bmp' || $(this).attr('data-extension') == 'gif') {
								//if(myApp.device.android){
								if (true) {
									PhotoViewer.show($(this).attr('src'));
								}
							} else {
								window.open('https://lk.urist.els24.com/api/file?token=' + localStorage.getItem('token') + '&id=' + $(this).attr('data-id'), '_system');
							}
						});
					}

					current_consultation.comments = msg.data;
					current_consultation.comments_count = msg.data.length;
					//localStorage.setItem("consultations", JSON.stringify( consultations ));
				}
			} else {}
		})
		.fail(function () {})
		.always(function () {});
}


function initChatInterface() {	
		
	initChatInterface.initClickBack();
	initChatInterface.initStatusbar();
	initChatInterface.initFeedback();
	initChatInterface.initHandlersState();
	initChatInterface.initKeyboardFocus();
	resetScrollChat();	
}

initChatInterface.initClickBack = function() {
	$('.js-chat-link-back').on('click', function (event) {
		var currentView = myApp.getCurrentView();
		currentView.router.back({
			url: 'consultations.html',
			force: true
		});
	});
}

initChatInterface.initStatusbar = function() {
	$('.js-chat-status').text(current_consultation.status_title);
	$('.js-chat-card').text(current_consultation.card_code);
	$('.js-chat-date').text(formatDate(current_consultation.created_at_ts, 'date'));
	$('.js-chat-country').text(current_consultation.low_country_title);
	$('.js-chat-type-request').text(current_consultation.services_title);
	$('.js-chat-content-request').text(current_consultation.description);

	$('.js-chat-statusbar').show();

	$('.js-chat-toggler').on('click', function() {
		$('.js-chat-statusbar').toggleClass('opened').animate({ scrollTop: 0 }, 0)
	})
}

var resetScrollChat = function(animatable) {	
		var $target = $('.js-chat-messages')		
		var node = $('.js-chat-messages').get(0)
		var scrollHeight = node.scrollHeight;		
		return $target.stop().animate({ scrollTop: scrollHeight}, (animatable ? 500 : 0))				
}

initChatInterface.initFeedback = function() {
	var currentStatus = Number(current_consultation.status_id);
	var needFeedback = currentStatus === 5 || currentStatus === 6;
	if (needFeedback) {
		$('.js-chat-feedback').show();
		$('.js-chat-page-content').addClass('short');				
	}	

	initRateStar()			
	initFeedAction.initComplaint()		
	initFeedAction.initThanks()
}

initChatInterface.initKeyboardFocus = function() {
	$('#commentMessage').on('focus', function() {
		resetScrollChat(true);
	})
}

initChatInterface.initHandlersState = function() {		
	var status = Number(current_consultation.status_id);
	// status 'done' or 'refused'
	if (status === 5 || status === 6) {		
		$('.js-chat-resume').show();
		$('.js-chat-messagebar').hide();
	}
	 
	if (status === 1) {
		$('.js-chat-cancel').show();
	}

	$('.js-chat-cancel').on('click', function() {
		myApp.confirm('Отклонить консультацию?', 'Подтвердите', function () {				
			$.ajax({
				method: "POST",
				url: cordovaMode ? "https://lk.urist.els24.com/api/cancel-consultation" : "echo_cancel_consultation.php",
				data: {
					'token': localStorage.getItem('token'),
					'task_id': current_consultation.id
				},
				timeout: 10000,
				beforeSend: function () {
					showLoading();
				}
			})
			.done(function (msg) {					
				if (msg.status == 'success') {
					$('.js-chat-cancel, .js-chat-messagebar').hide();
					$('.js-chat-resume').show();
					$('.js-chat-page-content').addClass('short');						
					resetScrollChat();					
					$('.js-chat-status').text("Отклонена");
					$('.js-chat-feedback').fadeIn(500);						

					myApp.alert('Консультация отменена', 'Спасибо');							
				} else {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				}
			})
			.fail(function () {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function () {
				hideLoading();
			});
		});			
	})

	$('.js-chat-resume').on('click', function() {				
		myApp.confirm('Возобновить консультацию?', 'Подтвердите', function () {										
			current_consultation.status_id = 2;

			myApp.alert('Пожалуйста, напишите причину возобновления консультации', 'Внимание', function() {				
				$('.js-chat-page-content').removeClass('short');				
				$('.js-chat-messagebar').show();
				$('.js-chat-feedback, .js-chat-resume').hide();
				resetScrollChat();
			});			
		});
	})
}

var initRateStar = function () {
	initRateStar.fillStars(current_consultation.rating);
	initRateStar.initClickStars()
}

initRateStar.fillStars = function(rate) {
	$('.star-rate__star').removeClass('active');
	$('.star-rate__star[data-rate=' + rate + ']').addClass('active').prevAll().addClass('active');
}

initRateStar.initClickStars = function() {
	$('.star-rate__star').on('click', function() {
		var rate = $(this).data('rate')						

		$.ajax({
			method: 'POST',
			url: cordovaMode ? "https://lk.urist.els24.com/api/rating" : "echo_rating.php",
				data: {
				token: localStorage.getItem('token'),
				task_id: current_consultation.id,
				rating: rate 
			},
			timeout: 10000,
			beforeSend: function() {
				initRateStar.fillStars(rate)
			}			
		})
		.done(function(res) {
			if (res.status !== 'success') {
				return myApp.alert('Оценка консультации не обновлена', 'Нет соединения с интернетом');
				initRateStar.fillStars(current_consultation.rating)
			}			
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом');
			initRateStar.fillStars(current_consultation.rating)
		})
		.always(function() {
			hideLoading()			
		})
	})
}

var initFeedAction = function(nameAction, $val, $btn) {
	if (current_consultation[nameAction]) {
		return $val.html(current_consultation[nameAction].description).show()		
	}

	window.addEventListener(nameAction + 'Added', function(e) {		
		$btn.hide();
		//e.detail is text from textarea
		$val.text(e.detail).show();
	})

	$btn.show().on('click', function() {
		$('.window.' + nameAction).fadeIn();
		showBg();

		if (myApp.device.android && cordovaMode) {
			var permissions = cordova.plugins.permissions;

			permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function (status) {
				if (!status.hasPermission) {
					permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE);
				}
			});
		}
	})
}

initFeedAction.initThanks = function() {
	initFeedAction('blagodarnost', $('.js-chat-val-thanks'), $('.js-chat-set-thanks'))	
}

initFeedAction.initComplaint = function() { 
	initFeedAction('pretenziya', $('.js-chat-val-complaint'), $('.js-chat-set-complaint'))
}
