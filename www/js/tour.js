myApp.onPageInit('tour', function (page) {
	buy_mode = 1;
	
	var tourSwiper = myApp.swiper('.swiper-container', {
		pagination:'.swiper-pagination'
	});
	
	$('.page[data-page=tour] .button').on('click',function(){
		localStorage.setItem('tour', true);
	});
	
	hideStatusBar();
});