var login_clicked = false;
var entered_pin = '';

var register_without_card = false;

myApp.onPageInit('login', function (page) {
	
	hideStatusBar();
	
	//$(".page[data-page=login] input[type=tel]").mask("+7(999)999-9999");
	Inputmask().mask("input");
	
	$('.page[data-page=login] input:not(.fixed):not([type=submit])').each(function(index, element) {
    $(element).on('focus',function(){
			$(element).parent().find('.title').addClass('active');
			$(element).addClass('active');
		});
		$(element).on('focusout',function(){
			if( $(element).val().length == 0 )
			{
				$(element).parent().find('.title').removeClass('active');
				$(element).removeClass('active');
			}
		});
    });
	
	$('.page[data-page=login] .bottom .button.buy').on('click',function(){
		register_without_card = false;
		
		$.ajax({
			method: "GET",
			url: cordovaMode?"https://urist.els24.com/geo/index.php":"echo_geoip.php",
			timeout: 30000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.result == "ok" )
			{
				localStorage.setItem("country", msg.data);

				$.ajax({
					method: "GET",
					url: cordovaMode?("https://lk.urist.els24.com/api/product?country=" + msg.data):"echo_products.php",
					data: {
						country: msg.data
					},
					timeout: 10000,
					beforeSend: function() {
						showLoading();
					}
				})
				.done(function( msg ) {
					console.log( msg );
					if( msg.status == "success" )
					{
						localStorage.setItem("products", JSON.stringify(msg.data));
						showStatusBar();
						buy_mode = 1;
						mainView.router.loadPage({
							url: 'buy.html',
							animatePages: true
						});
					}
					else{
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					}
				})
				.fail(function() {
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				})
				.always(function() {
					hideLoading();
				});
			}
			else{
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	});

	$('.page[data-page=login] .bottom .button.reg').on('click',function(){
		
		register_without_card = true;

		$.ajax({
			method: "GET",
			url: cordovaMode?"https://urist.els24.com/geo/index.php":"echo_geoip.php",
			timeout: 30000,
			beforeSend: function() {
				showLoading();
			}
		})
		.done(function( msg ) {
			console.log( msg );
			if( msg.result == "ok" )
			{
				localStorage.setItem("country", msg.data);

				mainView.router.loadPage({
					url: 'anketa.html',
					animatePages: true
				});
			}
			else{
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			}
		})
		.fail(function() {
			myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
		})
		.always(function() {
			hideLoading();
		});
	});
	
	$('.page[data-page=login] p').on('click',function(){
		if( $('.page[data-page=login] input[name=phone]').val().length == 0 )
		{
			$('.page[data-page=login] input[name=phone]').addClass('error');
			$('.page[data-page=login] input[name=phone]').focus();
			$('.page[data-page=login] input[name=phone]').parent().find('.hint').css('display','block');
		}else{
			$.ajax({
				method: "POST",
				url: cordovaMode?"https://lk.urist.els24.com/api/forgot-password":"echo_restore_password.php",
				//url: "echo_restore_password.php",
				data: { 
					phone: $('.page[data-page=login] input[name=phone]').val()
				},
				timeout: 10000,
				beforeSend: function() {
					showLoading();
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" )
				{
					myApp.alert('Новый пароль отправлен на указанный номер', 'Восстановление пароля', function () {});
				}
				else{
					myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				hideLoading();
			});
		}
	});
	
	$('.page[data-page=login] form').submit(function(event){
        event.preventDefault();
		
		$('.page[data-page=login] .hint').css('display','none');
		$('.page[data-page=login] input').removeClass('error');
		
		if( $('.page[data-page=login] input[name=phone]').val().length == 0 )
		{
			$('.page[data-page=login] input[name=phone]').addClass('error');
			$('.page[data-page=login] input[name=phone]').focus();
			$('.page[data-page=login] input[name=phone]').parent().find('.hint').css('display','block');
		}
		else if( $('.page[data-page=login] input[name=password]').val().length == 0 )
		{
			$('.page[data-page=login] input[name=password]').addClass('error');
			$('.page[data-page=login] input[name=password]').focus();
			$('.page[data-page=login] input[name=password]').parent().find('.hint').css('display','block');
		}
		else
		{
			$.ajax({
				method: "GET",
				url: cordovaMode?"https://lk.urist.els24.com/api/auth":"login_echo.php",
				//url: "login_echo.php",
				data: { 
					phone: $('.page[data-page=login] input[name=phone]').val(), 
					password: md5( $('.page[data-page=login] input[name=password]').val() + salt )
				},
				timeout: 10000,
				beforeSend: function() {
					$('.page[data-page=login] input[type=submit]').css('opacity',0.5);
					login_clicked = true;
					showLoading();
					
					savedPhone = $('.page[data-page=login] input[name=phone]').val();
					savedPass = md5( $('.page[data-page=login] input[name=password]').val() + salt );
				}
			})
			.done(function( msg ) {
				console.log( msg );
				if( msg.status == "success" )
				{
					loginStarted = false;
					
					localStorage.setItem('token', msg.token);
					localStorage.setItem('pass', savedPass);
					localStorage.setItem('phone', savedPhone);
					localStorage.setItem('name', msg.first_name);
					userFirstName = msg.first_name;
					
					localStorage.setItem('mb', msg.is_mb);

					if(!localStorage.getItem('pinRequested')){
						myApp.alert('Пожалуйста, установите PIN код для быстрого входа', 'Внимание', function () {
							changingPin=true;
							entered_pin='';
							$('.popup-pin .pindot').removeClass('active');
							myApp.popup('.popup.popup-pin');

							localStorage.setItem('pinRequested','true');
						});
					}else{
						mainView.router.loadPage({
							url: 'consultations.html',
							animatePages: true
						});
					}
				}
				else
				{
					myApp.alert('Пожалуйста, проверьте правильность введенных данных', 'Ошибка авторизации', function () {});
				}
			})
			.fail(function() {
				myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
			})
			.always(function() {
				$('.page[data-page=login] input[type=submit]').css('opacity',1);
				login_clicked = false;
				hideLoading();
			});
		}
    });
});

myApp.onPageBeforeAnimation('login', function (page) {
	hideStatusBar();
});