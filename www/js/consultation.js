var star_clicked = false;
var updateChatInterval;

var renewConsultation = false;

function setStars(value) {
	$('.stars .star').each(function (index, element) {
		if (parseInt($(element).attr('data-value')) <= parseInt(value)) {
			$(element).addClass('good');
		} else {
			$(element).removeClass('good');
		}
	});
}

function parseConsultation() {

	renewConsultation = false;

	$('.page[data-page=consultation] .status').text(current_consultation.status_title);
	$('.page[data-page=consultation] .date').text(formatDate(current_consultation.created_at_ts, 'date'));
	$('.page[data-page=consultation] .type').text(current_consultation.services_title);
	$('.page[data-page=consultation] .chat').text(current_consultation.comments.length + " сообщений в чате");
	$('.page[data-page=consultation] .country').text(current_consultation.low_country_title);
	$('.page[data-page=consultation] .description').text(current_consultation.description);
	$('.page[data-page=consultation] .cardnum').text(current_consultation.card_code);

	if (current_consultation.blagodarnost) {
		$('.page[data-page=consultation] .blagodarnost').html(current_consultation.blagodarnost.description);
	}

	if (current_consultation.pretenziya) {
		$('.page[data-page=consultation] .pretenziya').html(current_consultation.pretenziya.description);
	}

	setStars(current_consultation.rating);

	if (current_consultation.access_setting && current_consultation.access_setting.is_view_only == 1) {
		$('.page[data-page=consultation] .blagodarnost, .page[data-page=consultation] .pretenziya, .page[data-page=consultation] .stars').parent().addClass('hideit');
	} else {
		$('.page[data-page=consultation] .blagodarnost, .page[data-page=consultation] .pretenziya, .page[data-page=consultation] .stars').parent().removeClass('hideit');
	}

	if (current_consultation.access_setting && current_consultation.access_setting.is_managed == 1) {
		$('.hideConsultation').parent().removeClass('hideit');
	} else {
		$('.hideConsultation').parent().addClass('hideit');
	}

	$('.stars .star').each(function (index, element) {
		$(element).on('click', function () {
			if (current_consultation.status_id <= 2) {
				myApp.alert('Изменение оценки активной консультации невозможно.', 'Внимание', function () {});
			} else {
				if (!star_clicked) {
					setStars($(element).attr('data-value'));

					$.ajax({
							method: "POST",
							url: cordovaMode ? "https://lk.urist.els24.com/api/rating" : "echo_rating.php",
							data: {
								'token': localStorage.getItem('token'),
								'task_id': selected_consultation,
								'rating': $(element).attr('data-value')
							},
							timeout: 10000,
							beforeSend: function () {
								$('.stars').css('opacity', 0.5);
								star_clicked = true;
							}
						})
						.done(function (msg) {
							console.log(msg);
							if (msg.status == 'success') {
								myApp.alert('Оценка консультации обновлена', 'Спасибо', function () {});
							} else {
								myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
								setStars(current_consultation.rating);
							}
						})
						.fail(function () {
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
							setStars(current_consultation.rating);
						})
						.always(function () {
							$('.stars').css('opacity', 1.0);
							star_clicked = false;
						});
				}
			}
		});
	});

	$('.consNum').text(current_consultation.id);

	if (current_consultation.status_id == 5 || current_consultation.status_id == 6) {
		$('.cancelConsultation').css('display', 'none');
		$('.renewConsultation').css('display', 'block');
	} else {
		$('.renewConsultation').css('display', 'none');
		$('.cancelConsultation').css('display', 'block');
	}
}

myApp.onPageInit('consultation', function (page) {
	$('#newyear').fadeOut(1);

	$('[data-page="consultation"] .navbar .left .link').on('click', function () {
		var currentView = myApp.getCurrentView();
		currentView.router.back({
			url: 'consultations.html',
			force: true
		});
	});


	parseConsultation(selected_consultation);
	
	$('.page[data-page=consultation] .blagodarnost .button').on('click', function () {
		//$('.window.blagodarnost').removeClass('closed');

		if (current_consultation.status_id <= 2) {
			myApp.alert('Добавление благодарности активной консультации невозможно.', 'Внимание', function () {});
		} else {
			$('.window.blagodarnost').fadeIn();
			showBg();

			if (myApp.device.android && cordovaMode) {
				var permissions = cordova.plugins.permissions;

				permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function (status) {
					if (status.hasPermission) {} else {
						permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE);
					}
				});
			}
		}

	});

	$('.page[data-page=consultation] .pretenziya .button').on('click', function () {
		//$('.window.pretenziya').removeClass('closed');
		if (current_consultation.status_id <= 2) {
			myApp.alert('Добавление претензии активной консультации невозможно.', 'Внимание', function () {});
		} else {
			$('.window.pretenziya').fadeIn();
			showBg();

			if (myApp.device.android && cordovaMode) {
				var permissions = cordova.plugins.permissions;

				permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function (status) {
					if (status.hasPermission) {} else {
						permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE);
					}
				});
			}
		}
	});

	$('#vslider img').on('click', function () {
		if ($('#vslider').height() <= 60) {
			$('#vslider').css('max-height', '1000px');
		} else {
			$('#vslider').css('max-height', '60px');
		}
	});

	$('.cancelConsultation').on('click', function () {
		myApp.confirm('Отклонить консультацию?', 'Подтвердите',
			function () {
				$.ajax({
						method: "POST",
						url: cordovaMode ? "https://lk.urist.els24.com/api/cancel-consultation" : "echo_cancel_consultation.php",
						data: {
							'token': localStorage.getItem('token'),
							'task_id': current_consultation.id
						},
						timeout: 10000,
						beforeSend: function () {
							showLoading();
						}
					})
					.done(function (msg) {
						console.log(msg);
						if (msg.status == 'success') {
							myApp.alert('Консультация отменена', 'Спасибо', function () {});
							$('.cancelConsultation').remove();

							current_consultation.status_id = 6;
						} else {
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						}
					})
					.fail(function () {
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					})
					.always(function () {
						hideLoading();
					});
			},
			function () {}
		);
	});

	$('.hideConsultation .button').on('click', function () {
		myApp.confirm('Скрыть консультацию?', 'Подтвердите',
			function () {
				$.ajax({
						method: "POST",
						url: cordovaMode ? "https://lk.urist.els24.com/api/mb-hide-shared-task" : "echo_mb-hide-shared-task.php",
						data: {
							'token': localStorage.getItem('token'),
							'task_id': current_consultation.id
						},
						timeout: 10000,
						beforeSend: function () {
							showLoading();
						}
					})
					.done(function (msg) {
						console.log(msg);
						if (msg.status == 'success') {
							myApp.alert('Консультация скрыта', 'Спасибо', function () {});
							$('.hideConsultation').parent().addClass('hideit');
						} else {
							myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
						}
					})
					.fail(function () {
						myApp.alert('Пожалуйста, попробуйте еще раз', 'Нет соединения с интернетом', function () {});
					})
					.always(function () {
						hideLoading();
					});
			},
			function () {}
		);
	});

	$('.link.customback').on('click', function (event) {
		mainView.router.load({
			url: 'consultations.html',
			animatePages: false
		});
	});

	$('.button.renewConsultation').on('click', function (event) {
		myApp.alert('Пожалуйста, опишите причину возобновления консультации', 'Внимание', function () {

			renewConsultation = true;

			mainView.router.load({
				url: 'chat.html',
				animatePages: false
			});
		});
	});
});

myApp.onPageBeforeAnimation('consultation', function (page) {
	$('#mainToolbar').removeClass('toolbar-hidden');
	clearInterval(updateChatInterval);

	if (cordovaMode && myApp.device.ios) {
		NativeKeyboard.hideMessenger({
			animated: false
		});
	}

	if (renewConsultation) {
		$('.renewConsultation').remove();
	}

	parseConsultation();
});