var callSoundiOs;
var callTimerIOs;
var makeCall = true;
var appHasLoaded = false;

var current_version = 24;

var app = {
	
    initialize: function() {
        this.bindEvents();
    },
	
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
	
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
	
    receivedEvent: function(id) {
		cordovaMode = true;
		
		loadStartPage();
		
		window.ga.startTrackerWithId('UA-108823486-1');

		document.addEventListener("resume", onResume, false);
		document.addEventListener("pause", onPause, false);

		cordova.plugins.backgroundMode.enable();

		window.addEventListener('keyboardWillShow', function () {
            $('#mainToolbar').addClass('hidden_by_keyboard');
        });
        window.addEventListener('keyboardWillHide', function () {
            $('#mainToolbar').removeClass('hidden_by_keyboard');
        });
        window.addEventListener('keyboardDidShow', function () {
            
        });

        if(myApp.device.ios)
		{
			/*cordova.plugins.Keyboard.disableScroll(true);*/
			
			Keyboard.shrinkView(true);
			Keyboard.hideFormAccessoryBar(true);
			
/*
			StatusBar.overlaysWebView( false );
			StatusBar.backgroundColorByHexString('#24823c');
			StatusBar.styleDefault();
*/
			cordova.plugins.iosrtc.registerGlobals();

			SIP.WebRTC.isSupported = function isSupported() { return true; }
			SIP.WebRTC.MediaStream = cordova.plugins.iosrtc.MediaStream;
			SIP.WebRTC.getUserMedia = cordova.plugins.iosrtc.getUserMedia;
			SIP.WebRTC.RTCPeerConnection = cordova.plugins.iosrtc.RTCPeerConnection;
			SIP.WebRTC.RTCSessionDescription = cordova.plugins.iosrtc.RTCSessionDescription;

			var connectToSip = function() {
				if(call_has_started){
					
					makeCall = false;
					$('#connectToSipButtonDiv div').removeClass('endCall');

					var endCallTimeout = setTimeout(function(){
						clearTimeout(callTimerIOs);

						window.session.bye();

						callSoundiOs.pause();
						callSoundiOs.currentTime = 0;
					},50);

					//$('#earphone').css('opacity',1);
					
					$('#keyboard').css('opacity',0);
					$('#voicetarget').css('opacity',0);
					
					$('.precaller').fadeIn(300);

					myApp.closeModal();
					
					call_has_started = false;
				}else{
					
					var is_online = true;
					
					$.ajax({
						url: cordovaMode?"https://lk.urist.els24.com/api/education":"echo_education.php",
						timeout: 3000,
						statusCode: {
							0: function() {
								is_online = false;
								
								myApp.alert('Для совершения звонка требуется доступ к Интернет', 'Нет соединения с интернетом', function () {});
								
								makeCall = false;
								$('#connectToSipButtonDiv div').removeClass('endCall');

								var endCallTimeout = setTimeout(function(){
									clearTimeout(callTimerIOs);

									//window.session.bye();

									callSoundiOs.pause();
									callSoundiOs.currentTime = 0;
								},50);

								//$('#earphone').css('opacity',1);
								$('#keyboard').css('opacity',0);
								$('#voicetarget').css('opacity',0);

								//myApp.closeModal();

								call_has_started = false;
							}
						}
					});
					
					if(is_online){
						makeCall = true;
						call_has_started = true;

						$('#connectToSipButtonDiv div').addClass('endCall');

						callSoundiOs = new Audio('sounds/zvuki_gudkov_telefona.wav');
						callSoundiOs.loop = true;
						callSoundiOs.play();

						callTimerIOs = setTimeout(function(){
							if(premiumUser){
								var configuration = {
									traceSip: true,
									uri: '1013@188.94.208.133',
									//displayName: '1013',
									wsServers: [ "wss://freeswitch.intellin.ru:7443" ],
									password: '1013'
								};
							}else{
								var configuration = {
									traceSip: true,
									uri: '1013@188.94.208.133',
									//displayName: '1009',
									wsServers: [ "wss://freeswitch.intellin.ru:7443" ],
									password: '1013'
								};
							}


							window.userAgent = new SIP.UA( configuration );

							window.userAgent.on('connected', function()
							{
								cordova.plugins.iosrtc.getUserMedia(
								{
									audio: true, 
									video: false 
								},
								// success callback
								function (stream) 
								{
									console.log('got local MediaStream: ', stream);

									window.mediaStream = stream;

									var options = 
									{
										media: 
										{
											stream: window.mediaStream,
											constraints: 
											{
												audio: true,
												video: false
											},
											render: 
											{
												remote: document.getElementById( 'remoteVideo' ),
												local: document.getElementById( 'localVideo' )
											}
										}
									};

									if(makeCall && appHasLoaded){
										var profile = JSON.parse( localStorage.getItem("profile") );
										
										if(premiumUser){
											window.session = window.userAgent.invite( 'hot_els_i_pr_' + profile.phone + '@188.94.208.133', options);
										}else{
											window.session = window.userAgent.invite( 'hot_els_i_' + profile.phone + '@188.94.208.133', options);
										}


										if(window.userAgent.isRegistered){
											callSoundiOs.pause();
											callSoundiOs.currentTime = 0;

											$('.popup-call .caller .callbutton.dtmf').addClass('touchable');
											
											$('.precaller').fadeOut(300);
											$('#keyboard').css('opacity',1);
											$('#voicetarget').css('opacity',0.5);
										}
									}
								},
								// failure callback
								function (error) 
								{
									console.error('getUserMedia failed: ', error);
								});
							});
						},2000);

						window.userAgent.on('registered', function(){
							callSoundiOs.pause();
							callSoundiOs.currentTime = 0;

							makeCall = false;
							AudioToggle.setAudioMode(AudioToggle.EARPIECE);
						});

						window.userAgent.on('disconnected', function(){

							makeCall = false;
							$('#connectToSipButtonDiv div').removeClass('endCall');

							var endCallTimeout = setTimeout(function(){
								clearTimeout(callTimerIOs);

								//window.session.bye();

								callSoundiOs.pause();
								callSoundiOs.currentTime = 0;
							},50);

							//$('#earphone').css('opacity',1);
							$('#keyboard').css('opacity',0);
							$('#voicetarget').css('opacity',0);

							//myApp.closeModal();

							call_has_started = false;
						});
					}
				}
			};

			var connectToSipButton = document.getElementById( 'connectToSipButtonDiv' ); 

			connectToSipButton.addEventListener( "click", function() 
			{
				if ( typeof cordova !== 'undefined' ) 
				{
					cordova.plugins.diagnostic.requestMicrophoneAuthorization
					( 
						function( status ) 
						{
							if ( status === cordova.plugins.diagnostic.permissionStatus.GRANTED ) 
							{
								cordova.plugins.diagnostic.requestCameraAuthorization( 
									function(status) 
									{
										if ( status === cordova.plugins.diagnostic.permissionStatus.GRANTED ) 
										{
											connectToSip();
										}
									}, 
									function(error)
									{
										console.error("The following error occurred: "+error);
									},
									false 
								);
							}
						}, 
						function( error ) 
						{
							alert( 'Error while getting Microphone permissions.' );
							console.error( error );
						}
					);
				} 
				else 
				{
					connectToSip();
				}
			}, false );
			
			var andSessionButton = document.getElementById( 'endCall' );
			
			andSessionButton.addEventListener( "click", function(){
				makeCall = false;
				$('#connectToSipButtonDiv div').removeClass('endCall');
				
				var endCallTimeout = setTimeout(function(){
					clearTimeout(callTimerIOs);
				
					window.session.bye();

					callSoundiOs.pause();
					callSoundiOs.currentTime = 0;
				},50);
				
				//$('#earphone').css('opacity',1);
				$('#keyboard').css('opacity',0);
				$('#voicetarget').css('opacity',0);
				
				$('.precaller').fadeIn(300);
				
				myApp.closeModal();
				
				$('.popup-call .caller .callbutton.dtmf').removeClass('touchable');
			});
			
			var closeCallerButton = document.getElementById( 'closeCallerPopup' );
			
			closeCallerButton.addEventListener( "click", function(){
				makeCall = false;
				$('#connectToSipButtonDiv div').removeClass('endCall');
				
				var endCallTimeout = setTimeout(function(){
					clearTimeout(callTimerIOs);
				
					window.session.bye();

					callSoundiOs.pause();
					callSoundiOs.currentTime = 0;
				},50);
				
				//$('#earphone').css('opacity',1);
				$('#keyboard').css('opacity',0);
				$('#voicetarget').css('opacity',0);
				
				$('.precaller').fadeIn(300);
				
				myApp.closeModal();
				
				$('.popup-call .caller .callbutton.dtmf').removeClass('touchable');
			});
												
			var setMediaStream = function() 
			{
				if ( SIP.WebRTC.isSupported() ) 
				{
					var mediaConstraints = 
					{
						audio: true,
						video: false
					};

					var getUserMediaSuccess = function( stream ) 
					{
						window.mediaStream = stream;
					};

					var getUserMediaFail = function( e ) 
					{
						console.error( 'getUserMedia failed:', e );
					};

					window.userMediaPromise = SIP.WebRTC.getUserMedia( mediaConstraints );
				}
			};	
		}
		else
		{
			StatusBar.backgroundColorByHexString("#24823c");
			document.addEventListener("backbutton", onBackKeyDown, false);
			//AudioToggle.setAudioMode(AudioToggle.EARPIECE);
		}
		
		//loadStartPage();
    }
};

function onBackKeyDown(event) {
	goBack();
	event.preventDefault();
	return false;
}

//app.initialize();