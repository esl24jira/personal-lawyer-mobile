<?php
header("Content-type: application/json; charset=utf-8");

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($_POST)
    )
);

$context  = stream_context_create($options);

echo( file_get_contents('https://lk.urist.els24.com/api/support', false, $context) );
?>